import 'package:cheeers/src/core/models/drink.dart';
import 'package:cheeers/src/core/models/drink_filter.dart';
import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/core/models/ingredient.dart';
import 'package:cheeers/src/core/models/search_item.dart';
import 'package:cheeers/src/core/models/stralcoholic.dart';
import 'package:cheeers/src/core/models/strcategory.dart';
import 'package:cheeers/src/core/models/strglass.dart';
import 'package:cheeers/src/core/models/stringredient.dart';
import 'package:flutter/foundation.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class ApiService {
 
  static const String ALCOHOLIC_LIST_URL = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?a=list';
  static const String CATEGORY_LIST_URL = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list';
  static const String GLASS_LIST_URL = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?g=list';
  static const String INGREDIENTS_LIST_URL = 'https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list';

  static const String ALCOHOLIC_FILTER_URL = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=';
  static const String CATEGORY_FILTER_URL = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=';
  static const String GLASS_FILTER_URL = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?g=';
  static const String INGREDIENTS_FILTER_URL = 'https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=';

  static const String SEARCH_COCKTAIL_BY_NAME_URL = 'https://www.thecocktaildb.com/api/json/v1/1/search.php?s=';
  static const String SEARCH_INGREDIENT_BY_NAME_URL = 'https://www.thecocktaildb.com/api/json/v1/1/search.php?i=';

  static const String DRINK_DETAIL_BY_ID_URL = 'https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=';
  static const String INGREDIENT_DETAIL_BY_ID_URL = 'https://www.thecocktaildb.com/api/json/v1/1/lookup.php?iid=';

  static const String RANDOM_COCKTAIL_URL = 'https://www.thecocktaildb.com/api/json/v1/1/random.php';

  static const String INGREDIENT_THUMBNAIL_SMALL_URL = 'https://www.thecocktaildb.com/images/ingredients/{0}-Small.png';
  static const String INGREDIENT_THUMBNAIL_MEDIUM_URL = 'https://www.thecocktaildb.com/images/ingredients/{0}-Medium.png';
  static const String INGREDIENT_THUMBNAIL_LARGE_URL = 'https://www.thecocktaildb.com/images/ingredients/{0}.png';

  Future<List<StrAlcoholic>> getAlcoholicList() async {
    var response = await http.get(ALCOHOLIC_LIST_URL);
    if (response.statusCode == 200) {
      var drinks = convert.jsonDecode(response.body)['drinks'] as List;
      List<StrAlcoholic> alcoholicList = drinks.map((strAlcoholic) => StrAlcoholic.fromJson(strAlcoholic)).toList();
      return alcoholicList;
    } else {
      print('Request failed.');
      return null;
    }
  }

  Future<List<StrCategory>> getCategoriesList() async {
    var response = await http.get(CATEGORY_LIST_URL);
    if (response.statusCode == 200) {
      var drinks = convert.jsonDecode(response.body)['drinks'] as List;
      List<StrCategory> categoriesList = drinks.map((strCategory) => StrCategory.fromJson(strCategory)).toList();
      return categoriesList;
    } else {
      print('Request failed.');
      return null;
    }
  }

  Future<List<StrGlass>> getGlassesList() async {
    var response = await http.get(GLASS_LIST_URL);
    if (response.statusCode == 200) {
      var drinks = convert.jsonDecode(response.body)['drinks'] as List;
      List<StrGlass> glassesList = drinks.map((strGlass) => StrGlass.fromJson(strGlass)).toList();
      return glassesList;
    } else {
      print('Request failed.');
      return null;
    }
  }

  Future<List<StrIngredient1>> getIngredientsList() async {
    var response = await http.get(INGREDIENTS_LIST_URL);
    if (response.statusCode == 200) {
      var drinks = convert.jsonDecode(response.body)['drinks'] as List;
      List<StrIngredient1> ingredientsList = drinks.map((strIngredient) => StrIngredient1.fromJson(strIngredient)).toList();
      return ingredientsList;
    } else {
      print('Request failed.');
      return null;
    }
  }

  Future<List<DrinkFilter>> getDrinksFilterList({@required FilterType filterType, @required String filterValue}) async {
    String urlBase = '';
    switch(filterType) {
      case FilterType.alcoholic:
        urlBase = ALCOHOLIC_FILTER_URL;
        break;
      case FilterType.category:
        urlBase = CATEGORY_FILTER_URL;
        break;
      case FilterType.glass:
        urlBase = GLASS_FILTER_URL;
        break;
      case FilterType.ingredient:
        urlBase = INGREDIENTS_FILTER_URL;
        break;
      default:
        urlBase = '';
        break;
    }
    if (urlBase.isNotEmpty) {
      var url = '$urlBase$filterValue';
      var response = await http.get(url);
      if (response.statusCode == 200) {
        var drinks = convert.jsonDecode(response.body)['drinks'] as List;
        List<DrinkFilter> drinksList = drinks.map((drink) => DrinkFilter.fromJson(drink)).toList();
        return drinksList;
      } else {
        print('Request failed.');
        return null;
      }
    } else {
      return null;
    }
  }

  Future<List<Drink>> searchCocktailByName(String cocktailName) async {
    var response = await http.get('$SEARCH_COCKTAIL_BY_NAME_URL$cocktailName');
    if (response.statusCode == 200) {
      var drinks = convert.jsonDecode(response.body)['drinks'] as List;
      List<Drink> drinksList = drinks.map((drink) => Drink.fromJson(drink)).toList();
      return drinksList;
    } else {
      print('Request failed.');
      return null;
    }
  }

  Future<List<Ingredient>> searchIngredientByName(String ingredientName) async {
    var response = await http.get('$SEARCH_INGREDIENT_BY_NAME_URL$ingredientName');
    if (response.statusCode == 200) {
      var ingredients = convert.jsonDecode(response.body)['ingredients'] as List;
      List<Ingredient> ingredientsList = ingredients.map((ingredient) => Ingredient.fromJson(ingredient)).toList();
      return ingredientsList;
    } else {
      print('Request failed.');
      return null;
    }
  }

  Future<List<SearchItem>> searchCocktailByNameSearchItem(String cocktailName) async {
    var response = await http.get('$SEARCH_COCKTAIL_BY_NAME_URL$cocktailName');
    if (response.statusCode == 200) {
      var drinks = convert.jsonDecode(response.body)['drinks'] as List;
      List<SearchItem> drinksList = drinks?.map((drink) => Drink.fromJson(drink).toSearchItem())?.toList();
      return drinksList;
    } else {
      print('Request failed.');
      return null;
    }
  }

  Future<List<SearchItem>> searchIngredientByNameSearchItem(String ingredientName) async {
    var response = await http.get('$SEARCH_INGREDIENT_BY_NAME_URL$ingredientName');
    if (response.statusCode == 200) {
      var ingredients = convert.jsonDecode(response.body)['ingredients'] as List;
      List<SearchItem> ingredientsList = ingredients?.map((ingredient) => Ingredient.fromJson(ingredient).toSearchItem(INGREDIENT_THUMBNAIL_SMALL_URL))?.toList();
      return ingredientsList;
    } else {
      print('Request failed.');
      return null;
    }
  }

  Future<Drink> getDrinkDetailById(String drinkId) async {
    var response = await http.get('$DRINK_DETAIL_BY_ID_URL$drinkId');
    if (response.statusCode == 200) {
      var drinks = convert.jsonDecode(response.body)['drinks'] as List;
      List<Drink> drinksList = drinks.map((drink) => Drink.fromJson(drink)).toList();
      return drinksList.firstWhere((drink) => drink.idDrink == drinkId);
    } else {
      print('Request failed.');
      return null;
    }
  }
  
  Future<Ingredient> getIngredientDetailById(String ingredientId) async {
    var response = await http.get('$INGREDIENT_DETAIL_BY_ID_URL$ingredientId');
    if (response.statusCode == 200) {
      var ingredients = convert.jsonDecode(response.body)['ingredients'] as List;
      List<Ingredient> ingredientsList = ingredients.map((ingredient) => Ingredient.fromJson(ingredient)).toList();
      return ingredientsList.firstWhere((ingredient) => ingredient.idIngredient == ingredientId);
    } else {
      print('Request failed.');
      return null;
    }
  }

  Future<Drink> getRandomDrink() async {
    var response = await http.get(RANDOM_COCKTAIL_URL);
    if (response.statusCode == 200) {
      var drinks = convert.jsonDecode(response.body)['drinks'] as List;
      List<Drink> drinksList = drinks.map((drink) => Drink.fromJson(drink)).toList();
      return drinksList.first;
    } else {
      print('Request failed.');
      return null;
    }
  }

}