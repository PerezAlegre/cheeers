import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/core/models/search_item.dart';

class Ingredient {

  final String idIngredient;
  final String strIngredient;
  final String strDescription;
  final String strType;
  final String strAlcohol;
  final String strABV;
  
  Ingredient({
    this.idIngredient,
    this.strIngredient,
    this.strDescription,
    this.strType,
    this.strAlcohol,
    this.strABV,
  });

  Ingredient.fromJson(Map<String, dynamic> json)
    : idIngredient = json['idIngredient'],
      strIngredient = json['strIngredient'],
      strDescription = json['strDescription'],
      strType = json['strType'],
      strAlcohol = json['strAlcohol'],
      strABV = json['strABV'];

  SearchItem toSearchItem(String ingredientUrl) {
    return SearchItem(
      searchType: SearchType.ingredient,
      itemId: idIngredient,
      itemName: strIngredient,
      itemImageUrl: ingredientUrl.replaceFirst('{0}', strIngredient),
    );
  }

  @override
  String toString() {
    return '''{
  idIngredient: $idIngredient,
  strIngredient: $strIngredient,
  strDescription: $strDescription,
  strType: $strType,
  strAlcohol: $strAlcohol,
  strABV: $strABV
}''';
  }

}