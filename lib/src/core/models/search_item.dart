import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/utils/helper.dart';

class SearchItem {

  final SearchType searchType;
  final String itemId;
  final String itemName;
  final String itemImageUrl;

  SearchItem({
    this.searchType,
    this.itemId,
    this.itemName,
    this.itemImageUrl,
  });

  SearchItem.fromJson(Map<String, dynamic> json)
    : searchType = json['searchType'],
      itemId = json['itemId'],
      itemName = json['itemName'],
      itemImageUrl = json['itemImageUrl'];

  /// Used for deserializing string json object from SharedPreferences
  SearchItem.fromJsonPrefs(Map<String, dynamic> json)
    : searchType = Helper.mapIntToEnum<SearchType>(SearchType.values, int.parse(json['searchType'])),
      itemId = json['itemId'],
      itemName = json['itemName'],
      itemImageUrl = json['itemImageUrl'];

  Map<String, dynamic> toJson() => {
    'searchType' : searchType,
    'itemId' : itemId,
    'itemName' : itemName,
    'itemImageUrl' : itemImageUrl,
  };

  /// Used for saving this object into a json format to SharedPreferences
  Map<String, dynamic> toJsonPrefs() => {
    'searchType' : Helper.mapEnumToInt<SearchType>(SearchType.values, searchType).toString(),
    'itemId' : itemId,
    'itemName' : itemName,
    'itemImageUrl' : itemImageUrl,
  };

  @override
  String toString() {
    return '''{
  searchType: $searchType,
  itemId: $itemId,
  itemName: $itemName,
  itemImageUrl: $itemImageUrl
}''';
  }

}