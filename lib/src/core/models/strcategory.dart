class StrCategory {

  String strCategory;

  StrCategory({this.strCategory});

  StrCategory.fromJson(Map<String, dynamic> json)
    : strCategory = json['strCategory'];

  @override
  String toString() {
    return '{strCategory: $strCategory}';
  }
}