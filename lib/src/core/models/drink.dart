import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/core/models/search_item.dart';

class Drink {

  final String idDrink;
  final String strDrink;
  final String strDrinkAlternate;
  final String strDrinkES;
  final String strDrinkDE;
  final String strDrinkFR;
  final String strDrinkZHHANS;
  final String strDrinkZHHANT;
  final String strTags;
  final String strVideo;
  final String strCategory;
  final String strIBA;
  final String strAlcoholic;
  final String strGlass;
  final String strInstructions;
  final String strInstructionsES;
  final String strInstructionsDE;
  final String strInstructionsFR;
  final String strInstructionsZHHANS;
  final String strInstructionsZHHANT;
  final String strDrinkThumb;
  final String strIngredient1;
  final String strIngredient2;
  final String strIngredient3;
  final String strIngredient4;
  final String strIngredient5;
  final String strIngredient6;
  final String strIngredient7;
  final String strIngredient8;
  final String strIngredient9;
  final String strIngredient10;
  final String strIngredient11;
  final String strIngredient12;
  final String strIngredient13;
  final String strIngredient14;
  final String strIngredient15;
  final String strMeasure1;
  final String strMeasure2;
  final String strMeasure3;
  final String strMeasure4;
  final String strMeasure5;
  final String strMeasure6;
  final String strMeasure7;
  final String strMeasure8;
  final String strMeasure9;
  final String strMeasure10;
  final String strMeasure11;
  final String strMeasure12;
  final String strMeasure13;
  final String strMeasure14;
  final String strMeasure15;
  final String strCreativeCommonsConfirmed;
  final String dateModified;

  Drink({
    this.idDrink,
    this.strDrink,
    this.strDrinkAlternate,
    this.strDrinkES,
    this.strDrinkDE,
    this.strDrinkFR,
    this.strDrinkZHHANS,
    this.strDrinkZHHANT,
    this.strTags,
    this.strVideo,
    this.strCategory,
    this.strIBA,
    this.strAlcoholic,
    this.strGlass,
    this.strInstructions,
    this.strInstructionsES,
    this.strInstructionsDE,
    this.strInstructionsFR,
    this.strInstructionsZHHANS,
    this.strInstructionsZHHANT,
    this.strDrinkThumb,
    this.strIngredient1,
    this.strIngredient2,
    this.strIngredient3,
    this.strIngredient4,
    this.strIngredient5,
    this.strIngredient6,
    this.strIngredient7,
    this.strIngredient8,
    this.strIngredient9,
    this.strIngredient10,
    this.strIngredient11,
    this.strIngredient12,
    this.strIngredient13,
    this.strIngredient14,
    this.strIngredient15,
    this.strMeasure1,
    this.strMeasure2,
    this.strMeasure3,
    this.strMeasure4,
    this.strMeasure5,
    this.strMeasure6,
    this.strMeasure7,
    this.strMeasure8,
    this.strMeasure9,
    this.strMeasure10,
    this.strMeasure11,
    this.strMeasure12,
    this.strMeasure13,
    this.strMeasure14,
    this.strMeasure15,
    this.strCreativeCommonsConfirmed,
    this.dateModified,
  });

  Drink.fromJson(Map<String, dynamic> json)
    : idDrink = json['idDrink'],
      strDrink = json['strDrink'],
      strDrinkAlternate = json['strDrinkAlternate'],
      strDrinkES = json['strDrinkES'],
      strDrinkDE = json['strDrinkDE'],
      strDrinkFR = json['strDrinkFR'],
      strDrinkZHHANS = json['strDrinkZH-HANS'],
      strDrinkZHHANT = json['strDrinkZH-HANT'],
      strTags = json['strTags'],
      strVideo = json['strVideo'],
      strCategory = json['strCategory'],
      strIBA = json['strIBA'],
      strAlcoholic = json['strAlcoholic'],
      strGlass = json['strGlass'],
      strInstructions = json['strInstructions'],
      strInstructionsES = json['strInstructionsES'],
      strInstructionsDE = json['strInstructionsDE'],
      strInstructionsFR = json['strInstructionsFR'],
      strInstructionsZHHANS = json['strInstructionsZH-HANS'],
      strInstructionsZHHANT = json['strInstructionsZH-HANT'],
      strDrinkThumb = json['strDrinkThumb'],
      strIngredient1 = json['strIngredient1'],
      strIngredient2 = json['strIngredient2'],
      strIngredient3 = json['strIngredient3'],
      strIngredient4 = json['strIngredient4'],
      strIngredient5 = json['strIngredient5'],
      strIngredient6 = json['strIngredient6'],
      strIngredient7 = json['strIngredient7'],
      strIngredient8 = json['strIngredient8'],
      strIngredient9 = json['strIngredient9'],
      strIngredient10 = json['strIngredient10'],
      strIngredient11 = json['strIngredient11'],
      strIngredient12 = json['strIngredient12'],
      strIngredient13 = json['strIngredient13'],
      strIngredient14 = json['strIngredient14'],
      strIngredient15 = json['strIngredient15'],
      strMeasure1 = json['strMeasure1'],
      strMeasure2 = json['strMeasure2'],
      strMeasure3 = json['strMeasure3'],
      strMeasure4 = json['strMeasure4'],
      strMeasure5 = json['strMeasure5'],
      strMeasure6 = json['strMeasure6'],
      strMeasure7 = json['strMeasure7'],
      strMeasure8 = json['strMeasure8'],
      strMeasure9 = json['strMeasure9'],
      strMeasure10 = json['strMeasure10'],
      strMeasure11 = json['strMeasure11'],
      strMeasure12 = json['strMeasure12'],
      strMeasure13 = json['strMeasure13'],
      strMeasure14 = json['strMeasure14'],
      strMeasure15 = json['strMeasure15'],
      strCreativeCommonsConfirmed = json['strCreativeCommonsConfirmed'],
      dateModified = json['dateModified'];

  Map<String, dynamic> toJson() => {
    'idDrink' : idDrink,
    'strDrink' : strDrink,
    'strDrinkAlternate' : strDrinkAlternate,
    'strDrinkES' : strDrinkES,
    'strDrinkDE' : strDrinkDE,
    'strDrinkFR' : strDrinkFR,
    'strDrinkZHHANS' : strDrinkZHHANS,
    'strDrinkZHHANT' : strDrinkZHHANT,
    'strTags' : strTags,
    'strVideo' : strVideo,
    'strCategory' : strCategory,
    'strIBA' : strIBA,
    'strAlcoholic' : strAlcoholic,
    'strGlass' : strGlass,
    'strInstructions' : strInstructions,
    'strInstructionsES' : strInstructionsES,
    'strInstructionsDE' : strInstructionsDE,
    'strInstructionsFR' : strInstructionsFR,
    'strInstructionsZHHANS' : strInstructionsZHHANS,
    'strInstructionsZHHANT' : strInstructionsZHHANT,
    'strDrinkThumb' : strDrinkThumb,
    'strIngredient1' : strIngredient1,
    'strIngredient2' : strIngredient2,
    'strIngredient3' : strIngredient3,
    'strIngredient4' : strIngredient4,
    'strIngredient5' : strIngredient5,
    'strIngredient6' : strIngredient6,
    'strIngredient7' : strIngredient7,
    'strIngredient8' : strIngredient8,
    'strIngredient9' : strIngredient9,
    'strIngredient10' : strIngredient10,
    'strIngredient11' : strIngredient11,
    'strIngredient12' : strIngredient12,
    'strIngredient13' : strIngredient13,
    'strIngredient14' : strIngredient14,
    'strIngredient15' : strIngredient15,
    'strMeasure1' : strMeasure1,
    'strMeasure2' : strMeasure2,
    'strMeasure3' : strMeasure3,
    'strMeasure4' : strMeasure4,
    'strMeasure5' : strMeasure5,
    'strMeasure6' : strMeasure6,
    'strMeasure7' : strMeasure7,
    'strMeasure8' : strMeasure8,
    'strMeasure9' : strMeasure9,
    'strMeasure10' : strMeasure10,
    'strMeasure11' : strMeasure11,
    'strMeasure12' : strMeasure12,
    'strMeasure13' : strMeasure13,
    'strMeasure14' : strMeasure14,
    'strMeasure15' : strMeasure15,
    'strCreativeCommonsConfirmed' : strCreativeCommonsConfirmed,
    'dateModified' : dateModified,
  };

  SearchItem toSearchItem() {
    return SearchItem(
      searchType: SearchType.drink,
      itemId: idDrink,
      itemName: strDrink,
      itemImageUrl: strDrinkThumb,
    );
  }

  @override
  String toString() {
    return '''{
  idDrink: $idDrink,
  strDrink: $strDrink,
  strDrinkAlternate: $strDrinkAlternate,
  strDrinkES: $strDrinkES,
  strDrinkDE: $strDrinkDE,
  strDrinkFR: $strDrinkFR,
  strDrinkZHHANS: $strDrinkZHHANS,
  strDrinkZHHANT: $strDrinkZHHANT,
  strTags: $strTags,
  strVideo: $strVideo,
  strCategory: $strCategory,
  strIBA: $strIBA,
  strAlcoholic: $strAlcoholic,
  strGlass: $strGlass,
  strInstructions: $strInstructions,
  strInstructionsES: $strInstructionsES,
  strInstructionsDE: $strInstructionsDE,
  strInstructionsFR: $strInstructionsFR,
  strInstructionsZHHANS: $strInstructionsZHHANS,
  strInstructionsZHHANT: $strInstructionsZHHANT,
  strDrinkThumb: $strDrinkThumb,
  strIngredient1: $strIngredient1,
  strIngredient2: $strIngredient2,
  strIngredient3: $strIngredient3,
  strIngredient4: $strIngredient4,
  strIngredient5: $strIngredient5,
  strIngredient6: $strIngredient6,
  strIngredient7: $strIngredient7,
  strIngredient8: $strIngredient8,
  strIngredient9: $strIngredient9,
  strIngredient10: $strIngredient10,
  strIngredient11: $strIngredient11,
  strIngredient12: $strIngredient12,
  strIngredient13: $strIngredient13,
  strIngredient14: $strIngredient14,
  strIngredient15: $strIngredient15,
  strMeasure1: $strMeasure1,
  strMeasure2: $strMeasure2,
  strMeasure3: $strMeasure3,
  strMeasure4: $strMeasure4,
  strMeasure5: $strMeasure5,
  strMeasure6: $strMeasure6,
  strMeasure7: $strMeasure7,
  strMeasure8: $strMeasure8,
  strMeasure9: $strMeasure9,
  strMeasure10: $strMeasure10,
  strMeasure11: $strMeasure11,
  strMeasure12: $strMeasure12,
  strMeasure13: $strMeasure13,
  strMeasure14: $strMeasure14,
  strMeasure15: $strMeasure15,
  strCreativeCommonsConfirmed: $strCreativeCommonsConfirmed,
  dateModified: $dateModified,
}''';
  }

}
