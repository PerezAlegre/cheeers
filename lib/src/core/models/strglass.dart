class StrGlass {

  String strGlass;

  StrGlass({this.strGlass});

  StrGlass.fromJson(Map<String, dynamic> json)
    : strGlass = json['strGlass'];

  @override
  String toString() {
    return '{strGlass: $strGlass}';
  }
}