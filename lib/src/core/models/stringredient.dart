class StrIngredient1 {

  String strIngredient1;

  StrIngredient1({this.strIngredient1});

  StrIngredient1.fromJson(Map<String, dynamic> json)
    : strIngredient1 = json['strIngredient1'];

  @override
  String toString() {
    return '{strIngredient1: $strIngredient1}';
  }
}