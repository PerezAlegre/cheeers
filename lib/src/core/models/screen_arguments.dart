import 'package:cheeers/src/core/models/enums.dart';

class DrinksFilterListArguments {

  final FilterType filterType;
  final String drinkFilter;

  DrinksFilterListArguments({
    this.filterType,
    this.drinkFilter,
  });

}

class DrinkDetailArguments {

  final String drinkId;

  DrinkDetailArguments({
    this.drinkId
  });

}

class FavoriteDrinkArguments {

  final int initialPage;

  FavoriteDrinkArguments({
    this.initialPage
  });

}

class IngredientDetailArguments {

  final String strIngredient;

  IngredientDetailArguments({
    this.strIngredient
  });

}