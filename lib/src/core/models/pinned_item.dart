import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/utils/helper.dart';

class PinnedItem {

  final FilterType filterType;
  final String drinkFilter;

  PinnedItem({
    this.filterType,
    this.drinkFilter,
  });

  PinnedItem.fromJson(Map<String, dynamic> json)
    : filterType = json['filterType'],
      drinkFilter = json['drinkFilter'];

  /// Used for deserializing string json object from SharedPreferences
  PinnedItem.fromJsonPrefs(Map<String, dynamic> json)
    : filterType = Helper.mapIntToEnum<FilterType>(FilterType.values, int.parse(json['filterType'])),
      drinkFilter = json['drinkFilter'];

  Map<String, dynamic> toJson() => {
    'filterType' : filterType,
    'drinkFilter' : drinkFilter,
  };

  /// Used for saving this object into a json format to SharedPreferences
  Map<String, dynamic> toJsonPrefs() => {
    'filterType' : Helper.mapEnumToInt<FilterType>(FilterType.values, filterType).toString(),
    'drinkFilter' : drinkFilter,
  };

  @override
  String toString() {
    return '{ filterType: $filterType, drinkFilter: $drinkFilter }';
  }

  static List<PinnedItem> INITIAL_APP_DATA = [
    PinnedItem(filterType: FilterType.ingredient, drinkFilter: 'Jack Daniels'),
    PinnedItem(filterType: FilterType.ingredient, drinkFilter: 'Apple juice'),
    PinnedItem(filterType: FilterType.ingredient, drinkFilter: 'Chocolate'),
    PinnedItem(filterType: FilterType.ingredient, drinkFilter: 'Espresso'),
    PinnedItem(filterType: FilterType.ingredient, drinkFilter: 'Tequila'),
    PinnedItem(filterType: FilterType.ingredient, drinkFilter: 'Vodka'),
    PinnedItem(filterType: FilterType.category,   drinkFilter: 'Ordinary Drink'),
    PinnedItem(filterType: FilterType.category,   drinkFilter: 'Homemade Liqueur'),
    PinnedItem(filterType: FilterType.category,   drinkFilter: 'Coffee / Tea'),
    PinnedItem(filterType: FilterType.category,   drinkFilter: 'Beer'),
    PinnedItem(filterType: FilterType.category,   drinkFilter: 'Shot'),
    PinnedItem(filterType: FilterType.category,   drinkFilter: 'Cocktail'),
    PinnedItem(filterType: FilterType.glass,      drinkFilter: 'Coffee mug'),
    PinnedItem(filterType: FilterType.glass,      drinkFilter: 'Whiskey sour glass'),
    PinnedItem(filterType: FilterType.glass,      drinkFilter: 'Champagne flute'),
    PinnedItem(filterType: FilterType.glass,      drinkFilter: 'Old-fashioned glass'),
    PinnedItem(filterType: FilterType.glass,      drinkFilter: 'Highball glass'),
    PinnedItem(filterType: FilterType.glass,      drinkFilter: 'Cocktail glass'),
    PinnedItem(filterType: FilterType.alcoholic,  drinkFilter: 'Optional alcohol'),
    PinnedItem(filterType: FilterType.alcoholic,  drinkFilter: 'Non Alcoholic'),
    PinnedItem(filterType: FilterType.alcoholic,  drinkFilter: 'Alcoholic'),
  ];

}