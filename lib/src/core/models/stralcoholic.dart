class StrAlcoholic {

  String strAlcoholic;

  StrAlcoholic({this.strAlcoholic});

  StrAlcoholic.fromJson(Map<String, dynamic> json)
    : strAlcoholic = json['strAlcoholic'];

  @override
  String toString() {
    return '{strAlcoholic: $strAlcoholic}';
  }
}