enum FilterType {
  alcoholic,
  category,
  glass,
  ingredient,
}

enum SearchType {
  drink,        // values index [0]
  ingredient,   // values index [1]
}