class DrinkFilter {

  String strDrink;
  String strDrinkThumb;
  String idDrink;

  DrinkFilter({
    this.strDrink,
    this.strDrinkThumb,
    this.idDrink,
  });

  DrinkFilter.fromJson(Map<String, dynamic> json)
    : strDrink = json['strDrink'],
      strDrinkThumb = json['strDrinkThumb'],
      idDrink = json['idDrink'];

  @override
  String toString() {
    return '''{
  strDrink: $strDrink,
  strDrinkThumb: $strDrinkThumb,
  idDrink: $idDrink
}''';
  }

}