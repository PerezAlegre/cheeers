import 'package:flutter/material.dart';

class AppTheme {

  AppTheme._();

  static final ThemeData lightTheme = ThemeData(
    primaryColor: Colors.white,
    colorScheme: ColorScheme.light(
      primary: Colors.white,
      onPrimary: Colors.white,
      primaryVariant: Colors.grey[100],
      secondary: Colors.cyan[800],
      secondaryVariant: Colors.cyan[500],
      onSurface: Colors.purple[600],
    ),
    primaryIconTheme: IconThemeData(
      color: Colors.black,
    ),
    iconTheme: IconThemeData(
      color: Colors.black,
    ),
    unselectedWidgetColor: Colors.black,
    cursorColor: Colors.black,
    primaryTextTheme: TextTheme(
      headline1  : TextStyle(color: Colors.black54,),
      headline2  : TextStyle(color: Colors.black54,),
      headline3  : TextStyle(color: Colors.black54,),
      headline4  : TextStyle(color: Colors.black54,),
      headline5  : TextStyle(color: Colors.black87,),
      headline6  : TextStyle(color: Colors.black87, fontWeight: FontWeight.bold,   fontSize: 30.0),
      subtitle1  : TextStyle(color: Colors.black87,),
      bodyText1  : TextStyle(color: Colors.black87, fontWeight: FontWeight.bold,   fontSize: 20.0),
      bodyText2  : TextStyle(color: Colors.black87, fontWeight: FontWeight.normal, fontSize: 18.0),
      caption    : TextStyle(color: Colors.black54, fontWeight: FontWeight.normal, fontSize: 12.0),
      button     : TextStyle(color: Colors.black87,),
      subtitle2  : TextStyle(color: Colors.black, fontWeight: FontWeight.bold,   fontSize: 24.0),
      overline   : TextStyle(color: Colors.black,),
    ),
    textTheme: TextTheme(
      headline1  : TextStyle(color: Colors.black54,),
      headline2  : TextStyle(color: Colors.black54,),
      headline3  : TextStyle(color: Colors.black54,),
      headline4  : TextStyle(color: Colors.black54,),
      headline5  : TextStyle(color: Colors.black87,),
      headline6  : TextStyle(color: Colors.black87, fontWeight: FontWeight.normal,   fontSize: 20.0),
      subtitle1  : TextStyle(color: Colors.black87,),
      bodyText1  : TextStyle(color: Colors.black87,),
      bodyText2  : TextStyle(color: Colors.black87,),
      caption    : TextStyle(color: Colors.black54,),
      button     : TextStyle(color: Colors.black87,),
      subtitle2  : TextStyle(color: Colors.black,),
      overline   : TextStyle(color: Colors.black,),
    ),
    scaffoldBackgroundColor: Colors.white,
    appBarTheme: AppBarTheme(
      color: Colors.white,
      elevation: 0.0,
    ),
    cardTheme: CardTheme(
      color: Colors.white,
      elevation: 4.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
    ),
    toggleableActiveColor: Colors.cyan[800],
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Colors.cyan[800],
      foregroundColor: Colors.white,
    ),
    buttonColor: Colors.cyan[800],
    buttonTheme: ButtonThemeData(
      buttonColor: Colors.cyan[800],
      focusColor: Colors.cyan[800],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
    ),
    dialogTheme: DialogTheme(
      backgroundColor: Colors.white,
      titleTextStyle: TextStyle(
        color: Colors.black
      ),
      contentTextStyle: TextStyle(
        color: Colors.black
      ),
    ),
    snackBarTheme: SnackBarThemeData(
      backgroundColor: Colors.black,
      behavior: SnackBarBehavior.floating,
      contentTextStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 18.0),
      actionTextColor: Colors.cyanAccent[400],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
    ),
    hintColor: Colors.black38,
    inputDecorationTheme: InputDecorationTheme(
      hintStyle: TextStyle(color: Colors.black38, fontWeight: FontWeight.normal, fontSize: 18.0),
      labelStyle: TextStyle(color: Colors.black, fontWeight: FontWeight.normal, fontSize: 18.0),
    ),
    dividerColor: Colors.grey[100],
    dividerTheme: DividerThemeData(
      color: Colors.grey[100],
      thickness: 1.0,
    ),
  );
  
  static final ThemeData darkTheme = ThemeData(
    primaryColor: Colors.grey[900],
    colorScheme: ColorScheme.light(
      primary: Colors.grey[900],
      onPrimary: Colors.grey[900],
      primaryVariant: Colors.grey[800],
      secondary: Colors.cyanAccent[400],
      secondaryVariant: Colors.cyan[100],
      onSurface: Colors.purple[200],
    ),
    primaryIconTheme: IconThemeData(
      color: Colors.white,
    ),
    iconTheme: IconThemeData(
      color: Colors.white,
    ),
    unselectedWidgetColor: Colors.grey,
    cursorColor: Colors.grey,
    primaryTextTheme: TextTheme(
      headline1  : TextStyle(color: Colors.white70,),
      headline2  : TextStyle(color: Colors.white70,),
      headline3  : TextStyle(color: Colors.white70,),
      headline4  : TextStyle(color: Colors.white70,),
      headline5  : TextStyle(color: Colors.white,),
      headline6  : TextStyle(color: Colors.white, fontWeight: FontWeight.bold,   fontSize: 30.0),
      subtitle1  : TextStyle(color: Colors.white,),
      bodyText1  : TextStyle(color: Colors.white, fontWeight: FontWeight.bold,   fontSize: 20.0),
      bodyText2  : TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 18.0),
      caption    : TextStyle(color: Colors.white70, fontWeight: FontWeight.normal, fontSize: 12.0),
      button     : TextStyle(color: Colors.white,),
      subtitle2  : TextStyle(color: Colors.white, fontWeight: FontWeight.bold,   fontSize: 24.0),
      overline   : TextStyle(color: Colors.white,),
    ),
    textTheme: TextTheme(
      headline1  : TextStyle(color: Colors.white70,),
      headline2  : TextStyle(color: Colors.white70,),
      headline3  : TextStyle(color: Colors.white70,),
      headline4  : TextStyle(color: Colors.white70,),
      headline5  : TextStyle(color: Colors.white,),
      headline6  : TextStyle(color: Colors.white, fontWeight: FontWeight.normal,   fontSize: 20.0),
      subtitle1  : TextStyle(color: Colors.white,),
      bodyText1  : TextStyle(color: Colors.white,),
      bodyText2  : TextStyle(color: Colors.white,),
      caption    : TextStyle(color: Colors.white70,),
      button     : TextStyle(color: Colors.white,),
      subtitle2  : TextStyle(color: Colors.white,),
      overline   : TextStyle(color: Colors.white,),
    ),
    scaffoldBackgroundColor: Colors.grey[900],
    appBarTheme: AppBarTheme(
      color: Colors.grey[900],
      elevation: 0.0,
    ),
    cardTheme: CardTheme(
      color: Colors.grey[800],
      elevation: 4.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
    ),
    toggleableActiveColor: Colors.cyanAccent[400],
    floatingActionButtonTheme: FloatingActionButtonThemeData(
      backgroundColor: Colors.cyanAccent[400],
      foregroundColor: Colors.grey[900],
    ),
    buttonColor: Colors.cyanAccent[400],
    buttonTheme: ButtonThemeData(
      buttonColor: Colors.cyanAccent[400],
      focusColor: Colors.cyanAccent[400],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 20.0),
    ),
    dialogTheme: DialogTheme(
      backgroundColor: Colors.grey[900],
      titleTextStyle: TextStyle(
        color: Colors.white
      ),
      contentTextStyle: TextStyle(
        color: Colors.white
      ),
    ),
    snackBarTheme: SnackBarThemeData(
      backgroundColor: Colors.white,
      behavior: SnackBarBehavior.floating,
      contentTextStyle: TextStyle(color: Colors.grey[900], fontWeight: FontWeight.normal, fontSize: 18.0),
      actionTextColor: Colors.cyanAccent[800],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
    ),
    hintColor: Colors.white54,
    inputDecorationTheme: InputDecorationTheme(
      hintStyle: TextStyle(color: Colors.white54, fontWeight: FontWeight.normal, fontSize: 18.0),
      labelStyle: TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 18.0),
    ),
    dividerColor: Colors.grey[800],
    dividerTheme: DividerThemeData(
      color: Colors.grey[800],
      thickness: 1.0,
    ),
  );

}