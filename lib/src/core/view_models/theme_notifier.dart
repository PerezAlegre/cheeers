import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeNotifier with ChangeNotifier {

  ThemeMode _themeMode;

  ThemeNotifier() {
    _getDataFromSharedPreferences();
  }

  ThemeMode get themeMode => _themeMode;

  void setTheme(ThemeMode theme) {
    _themeMode = theme;
    _updateSharedPreferences();
    notifyListeners();
  }

  Future _getDataFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var result = prefs.getBool('theme');
    if (result != null) {
      _themeMode = result == true ? ThemeMode.dark : ThemeMode.light;
    } else {
      _themeMode = ThemeMode.light;
      _updateSharedPreferences();
    }
    notifyListeners();
  }

  Future _updateSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // value = true  => dark
    // value = false => light
    bool value = _themeMode == ThemeMode.dark ? true : false;
    await prefs.setBool('theme', value);
  }
}