import 'package:cheeers/src/core/models/drink.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:flutter/material.dart';

class DrinkProvider with ChangeNotifier {

  final _apiService = ApiService();

  DrinkProvider(String drinkId) {
    getDrinkDetailById(drinkId);
  }

  Drink _drink;

  Drink get drink => _drink;

  void _setDrink(Drink drink) {
    _drink = drink;
    notifyListeners();
  }

  Future<void> getDrinkDetailById(String drinkId) async {
    var drink = await _apiService.getDrinkDetailById(drinkId);
    _setDrink(drink);
  }

  @override
  void dispose() {
    super.dispose();
  }

}