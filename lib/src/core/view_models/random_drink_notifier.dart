import 'package:cheeers/src/core/models/drink.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:flutter/material.dart';

class RandomDrinkNotifier with ChangeNotifier {

  final _apiService = ApiService();

  RandomDrinkNotifier();

  Drink _randomDrink;

  Drink get randomDrink => _randomDrink;

  void _setRandomDrink(Drink drink) {
    _randomDrink = drink;
    notifyListeners();
  }

  Future<void> getRandomDrink() async {
    var drink = await _apiService.getRandomDrink();
    _setRandomDrink(drink);
  }

  @override
  void dispose() {
    super.dispose();
  }

}