import 'package:cheeers/src/core/models/stringredient.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:flutter/material.dart';

class IngredientsListNotifier with ChangeNotifier {

  final _apiService = ApiService();

  IngredientsListNotifier() {
    getAllIngredientsList();
  }

  List<StrIngredient1> _ingredientsList;

  List<StrIngredient1> get ingredientsList => _ingredientsList;

  void _setIngredientsList(List<StrIngredient1> ingredientsList) {
    _ingredientsList = ingredientsList;
    notifyListeners();
  }

  Future<void> getAllIngredientsList() async {
    var result = await _apiService.getIngredientsList();
    result.removeWhere((x) => x.strIngredient1 == null || x.strIngredient1.isEmpty);
    result.sort((a, b) => a.strIngredient1.toLowerCase().compareTo(b.strIngredient1.toLowerCase()));
    var list = result;
    _setIngredientsList(list);
  }

  @override
  void dispose() {
    super.dispose();
  }

}