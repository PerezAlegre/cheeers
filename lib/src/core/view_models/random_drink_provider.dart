import 'package:cheeers/src/core/models/drink.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:flutter/material.dart';

class RandomDrinkProvider with ChangeNotifier {

  final _apiService = ApiService();

  RandomDrinkProvider() {
    getRandomDrink();
  }

  Drink _drink;

  Drink get drink => _drink;

  void _setDrink(Drink drink) {
    _drink = drink;
    notifyListeners();
  }

  Future<void> getRandomDrink() async {
    var drink = await _apiService.getRandomDrink();
    _setDrink(drink);
  }

  @override
  void dispose() {
    super.dispose();
  }

}