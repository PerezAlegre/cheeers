import 'dart:convert' as convert;
import 'package:cheeers/src/core/models/drink.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FavoritesListNotifier with ChangeNotifier {

  FavoritesListNotifier() {
    _getDataFromSharedPreferences();
  }

  List<Drink> _favoritesList = [];

  List<Drink> get favoritesList => _favoritesList.reversed.toList();

  void _setFavoritesList(List<Drink> favoritesList) {
    _favoritesList = favoritesList;
    notifyListeners();
  }

  Future _getDataFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var result = prefs.getStringList('favorites');
    if (result != null) {
      var valuesList = result.map((drink) => Drink.fromJson(convert.jsonDecode(drink))).toList();
      _setFavoritesList(valuesList);
    } else {
      _favoritesList = [];
      _updateSharedPreferences();
    }
    notifyListeners();
  }

  Future _updateSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var favoritesStringList = _favoritesList.map((value) => convert.jsonEncode(value.toJson())).toList();
    await prefs.setStringList('favorites', favoritesStringList);
  }

  void addFavorite(Drink drink) {
    if (drink != null) {
      _favoritesList.add(drink);
      _setFavoritesList(_favoritesList);
      _updateSharedPreferences();
    }
  }

  void removeFavorite(Drink drink) {
    if (drink != null) {
      _favoritesList.removeWhere((value) => value.idDrink == drink.idDrink);
      _setFavoritesList(_favoritesList);
      _updateSharedPreferences();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

}