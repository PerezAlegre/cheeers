import 'package:cheeers/src/core/models/strglass.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:flutter/material.dart';

class GlassesListNotifier with ChangeNotifier {

  final _apiService = ApiService();

  GlassesListNotifier() {
    getAllGlassesList();
  }

  List<StrGlass> _glassesList;

  List<StrGlass> get glassesList => _glassesList;

  void _setGlassesList(List<StrGlass> glassesList) {
    _glassesList = glassesList;
    notifyListeners();
  }

  Future<void> getAllGlassesList() async {
    var result = await _apiService.getGlassesList();
    result.removeWhere((x) => x.strGlass == null || x.strGlass.isEmpty);
    result.sort((a, b) => a.strGlass.toLowerCase().compareTo(b.strGlass.toLowerCase()));
    var list = result;
    _setGlassesList(list);
  }

  @override
  void dispose() {
    super.dispose();
  }

}