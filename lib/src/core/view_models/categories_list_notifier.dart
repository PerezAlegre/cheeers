import 'package:cheeers/src/core/models/strcategory.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:flutter/material.dart';

class CategoriesListNotifier with ChangeNotifier {

  final _apiService = ApiService();

  CategoriesListNotifier() {
    getAllCategoriesList();
  }

  List<StrCategory> _categoriesList;

  List<StrCategory> get categoriesList => _categoriesList;

  void _setCategoriesList(List<StrCategory> categoriesList) {
    _categoriesList = categoriesList;
    notifyListeners();
  }

  Future<void> getAllCategoriesList() async {
    var result = await _apiService.getCategoriesList();
    result.removeWhere((x) => x.strCategory == null || x.strCategory.isEmpty);
    result.sort((a, b) => a.strCategory.toLowerCase().compareTo(b.strCategory.toLowerCase()));
    var list = result;
    _setCategoriesList(list);
  }

  @override
  void dispose() {
    super.dispose();
  }

}