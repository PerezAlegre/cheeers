import 'dart:convert' as convert;
import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/core/models/pinned_item.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PinnedItemsNotifier with ChangeNotifier {

  PinnedItemsNotifier() {
    _getDataFromSharedPreferences();
  }

  List<PinnedItem> _pinnedItemList = [];

  List<PinnedItem> get pinnedItemList => _pinnedItemList;

  List<PinnedItem> get pinnedItemListIngredients => _pinnedItemList.where((item) => item.filterType == FilterType.ingredient).toList().reversed.toList();
  List<PinnedItem> get pinnedItemListCategories  => _pinnedItemList.where((item) => item.filterType == FilterType.category).toList().reversed.toList();
  List<PinnedItem> get pinnedItemListGlasses     => _pinnedItemList.where((item) => item.filterType == FilterType.glass).toList().reversed.toList();
  List<PinnedItem> get pinnedItemListAlcoholic   => _pinnedItemList.where((item) => item.filterType == FilterType.alcoholic).toList().reversed.toList();

  void _setPinnedItemList(List<PinnedItem> pinnedItemList) {
    _pinnedItemList = pinnedItemList;
    notifyListeners();
  }

  Future _getDataFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('pinnedItems')) {
      _setInitialAppData();
    } else {
      var result = prefs.getStringList('pinnedItems');
      if (result != null) {
        var valuesList = result.map((pinnedItem) => PinnedItem.fromJsonPrefs(convert.jsonDecode(pinnedItem))).toList();
        _setPinnedItemList(valuesList);
      } else {
        _pinnedItemList = [];
        _updateSharedPreferences();
      }
    }
    notifyListeners();
  }

  Future _updateSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var pinnedItemStringList = _pinnedItemList.map((value) => convert.jsonEncode(value.toJsonPrefs())).toList();
    await prefs.setStringList('pinnedItems', pinnedItemStringList);
  }

  void _setInitialAppData() {
    _pinnedItemList = PinnedItem.INITIAL_APP_DATA;
    _updateSharedPreferences();
  }

  void addPinnedItem(PinnedItem pinnedItem) {
    if (pinnedItem != null) {
      _pinnedItemList.add(pinnedItem);
      _setPinnedItemList(_pinnedItemList);
      _updateSharedPreferences();
    }
  }

  void removePinnedItem(PinnedItem pinnedItem) {
    if (pinnedItem != null) {
      _pinnedItemList.removeWhere((value) => value.drinkFilter == pinnedItem.drinkFilter && value.filterType == pinnedItem.filterType);
      _setPinnedItemList(_pinnedItemList);
      _updateSharedPreferences();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

}