import 'dart:convert' as convert;
import 'package:cheeers/src/core/models/search_item.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchNotifier with ChangeNotifier {

  final _apiService = ApiService();

  SearchNotifier() {
    _getDataFromSharedPreferences();
  }

  List<SearchItem> _historySearchList = [];

  List<SearchItem> get historySearchList => _historySearchList.reversed.toList().take(15).toList();

  void _setHistorySearchList(List<SearchItem> historySearchList) {
    _historySearchList = historySearchList;
    notifyListeners();
  }

  List<SearchItem> _searchItemList;

  List<SearchItem> get searchItemList => _searchItemList;

  void _setSearchItemList(List<SearchItem> searchItemList) {
    _searchItemList = searchItemList;
    notifyListeners();
  }

  Future<void> searchItems(String query) async {
    var results = await Future.wait([
      _apiService.searchCocktailByNameSearchItem(query),
      _apiService.searchIngredientByNameSearchItem(query),
    ]);
    var searchItemList = results?.first?.toList();
    searchItemList?.addAll(results?.last?.toList());
    _setSearchItemList(searchItemList);
  }

  void updateHistorySearchList(SearchItem searchItem) {
    if (searchItem != null) {
      if (_historySearchList.contains(searchItem)) {
        int index = _historySearchList.indexOf(searchItem);
        _historySearchList.removeAt(index);
        _historySearchList.add(searchItem);
      } else {
        _historySearchList.add(searchItem);
      }
      _setHistorySearchList(_historySearchList);
      _updateSharedPreferences();
    }
  }
  
  Future _getDataFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var result = prefs.getStringList('searchHistory');
    if (result != null) {
      var valuesList = result.map((searchItem) => SearchItem.fromJsonPrefs(convert.jsonDecode(searchItem))).toList();
      _setHistorySearchList(valuesList);
    } else {
      _historySearchList = [];
      _updateSharedPreferences();
    }
    notifyListeners();
  }

  Future _updateSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var searchHistoryStringList = _historySearchList.map((value) => convert.jsonEncode(value.toJsonPrefs())).toList();
    await prefs.setStringList('searchHistory', searchHistoryStringList);
  }

  @override
  void dispose() {
    super.dispose();
  }

}