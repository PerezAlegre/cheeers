import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageNotifier with ChangeNotifier {

  LanguageEnum _language;

  String _languageStr;

  LanguageNotifier() {
    _getDataFromSharedPreferences();
  }

  LanguageEnum get language => _language;
  
  String get languageStr => _languageStr;

  void setLanguage(LanguageEnum language) {
    _language = language;
    _updateSharedPreferences();
    _setLanguageStr();
    notifyListeners();
  }

  void _setLanguageStr() {
    switch (_language) {
      case LanguageEnum.en:
        _languageStr = 'English';
        break;
      case LanguageEnum.es:
        _languageStr = 'Español';
        break;
      default:
        _languageStr = 'English';
        break;
    }
  }

  Future _getDataFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var result = prefs.getString('language');
    if (result != null) {
      switch (result) {
        case 'en':
          _language = LanguageEnum.en;
          break;
        case 'es':
          _language = LanguageEnum.es;
          break;
        default:
          _language = LanguageEnum.en;
          break;
      }
    } else {
      _language = LanguageEnum.en;
      _updateSharedPreferences();
    }
    _setLanguageStr();
    notifyListeners();
  }

  Future _updateSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String value;
    switch (_language) {
      case LanguageEnum.en:
        value = 'en';
        break;
      case LanguageEnum.es:
        value = 'es';
        break;
      default:
        value = 'en';
        break;
    }
    await prefs.setString('language', value);
  }

}

enum LanguageEnum {
  en,
  es,
}