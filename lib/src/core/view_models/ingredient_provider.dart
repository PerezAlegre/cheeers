import 'package:cheeers/src/core/models/ingredient.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:flutter/material.dart';

class IngredientProvider with ChangeNotifier {

  final _apiService = ApiService();

  IngredientProvider(String strIngredient) {
    getIngredientDetailByName(strIngredient);
  }

  Ingredient _ingredient;

  Ingredient get ingredient => _ingredient;

  void _setIngredient(Ingredient ingredient) {
    _ingredient = ingredient;
    notifyListeners();
  }

  Future<void> getIngredientDetailByName(String strIngredient) async {
    var ingredientsList = await _apiService.searchIngredientByName(strIngredient);
    var ingredientsFiltered = ingredientsList.where((value) => value.strIngredient.trim().toLowerCase() == strIngredient.trim().toLowerCase()).toList();
    if (ingredientsFiltered.length == 1) {
      _setIngredient(ingredientsFiltered.first);
    } else {
      _setIngredient(null);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

}