import 'package:cheeers/src/core/models/stralcoholic.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:flutter/material.dart';

class AlcoholicListNotifier with ChangeNotifier {

  final _apiService = ApiService();

  AlcoholicListNotifier() {
    getAllAlcoholicList();
  }

  List<StrAlcoholic> _alcoholicsList;

  List<StrAlcoholic> get alcoholicsList => _alcoholicsList;

  void _setAlcoholicList(List<StrAlcoholic> alcoholicsList) {
    _alcoholicsList = alcoholicsList;
    notifyListeners();
  }

  Future<void> getAllAlcoholicList() async {
    var result = await _apiService.getAlcoholicList();
    result.removeWhere((x) => x.strAlcoholic == null || x.strAlcoholic.isEmpty);
    result.sort((a, b) => a.strAlcoholic.toLowerCase().compareTo(b.strAlcoholic.toLowerCase()));
    var list = result;
    _setAlcoholicList(list);
  }

  @override
  void dispose() {
    super.dispose();
  }

}