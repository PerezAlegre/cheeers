import 'package:cheeers/src/core/models/drink_filter.dart';
import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:flutter/material.dart';

class DrinksFilterListNotifier with ChangeNotifier {

  final _apiService = ApiService();

  DrinksFilterListNotifier();

  List<DrinkFilter> _drinksFilterList;

  List<DrinkFilter> get drinksFilterList => _drinksFilterList;

  void _setDrinksFilterList(List<DrinkFilter> drinksFilterList) {
    _drinksFilterList = drinksFilterList;
    notifyListeners();
  }

  Future<void> getAllDrinksList({@required FilterType filterType, @required String filterValue}) async {
    var result = await _apiService.getDrinksFilterList(
      filterType: filterType,
      filterValue: filterValue
    );
    result.removeWhere((x) => x.idDrink == null || x.idDrink.isEmpty);
    result.sort((a, b) => a.strDrink.toLowerCase().compareTo(b.strDrink.toLowerCase()));
    var list = result;
    _setDrinksFilterList(list);
  }

  @override
  void dispose() {
    super.dispose();
  }

}