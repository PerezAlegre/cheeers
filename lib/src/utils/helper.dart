class Helper {

  static String replaceSpaceToUnderscore(String text) {
    if (text != null) {
      return text.replaceAll(' ', '_');
    }
    return text;
  }

  /// Convert enum value to int
  static int mapEnumToInt<T>(List<T> enumValues, T value) {
    if (value == null) {
      return null;
    }
    return enumValues.indexOf(value);
  }

  /// Convert int value to a Enum value
  static T mapIntToEnum<T>(List<T> enumValues, int value) {
    if (value == null) {
      return null;
    }
    return enumValues[value];
  }

}