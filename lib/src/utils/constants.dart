import 'package:cheeers/src/core/view_models/language_notifier.dart';

class ConstantsRoutes {

  static const String splashScreenRoute = '/splashScreen';
  static const String homeRoute = '/home';
  static const String settingsRoute = '/settings';
  static const String exploreRoute = '/explore';
  static const String searchRoute = '/search';
  static const String favoritesRoute = '/favorites';
  static const String allAlcoholicsRoute = '/allAlcoholics';
  static const String allCategoriesRoute = '/allCategories';
  static const String allGlassesRoute = '/allGlasses';
  static const String allIngredientsRoute = '/allIngredients';
  static const String drinksFilterRoute = '/drinksFilter';
  static const String drinkDetailRoute = '/drinkDetail';
  static const String favoriteDrinkRoute = '/favoriteDrink';
  static const String ingredientDetailRoute = '/ingredientDetail';
  static const String randomDrinkRoute = '/randomDrink';
  static const String appInfoRoute = '/appInfo';

}

class ConstantsStrings {

  LanguageEnum language;

  ConstantsStrings(LanguageEnum language) {
    this.language = language;
  }

  String searchStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Search';
        break;
      case LanguageEnum.es:
        return 'Buscar';
        break;
      default:
        return 'Search';
        break;
    }
  }

  String hintSearchStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Drinks or ingredients';
        break;
      case LanguageEnum.es:
        return 'Tragos o ingredientes';
        break;
      default:
        return 'Drinks or ingredients';
        break;
    }
  }

  String searchDrinksWithStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Search drinks with';
        break;
      case LanguageEnum.es:
        return 'Buscar tragos con';
        break;
      default:
        return 'Search drinks with';
        break;
    }
  }

  String settingsStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Settings';
        break;
      case LanguageEnum.es:
        return 'Configuración';
        break;
      default:
        return 'Settings';
        break;
    }
  }

  String darkModeStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Dark mode';
        break;
      case LanguageEnum.es:
        return 'Modo oscuro';
        break;
      default:
        return 'Dark mode';
        break;
    }
  }

  String languageStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Language';
        break;
      case LanguageEnum.es:
        return 'Idioma';
        break;
      default:
        return 'Language';
        break;
    }
  }

  String cancelStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Cancel';
        break;
      case LanguageEnum.es:
        return 'Cancelar';
        break;
      default:
        return 'Cancel';
        break;
    }
  }

  String okStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Ok';
        break;
      case LanguageEnum.es:
        return 'Aceptar';
        break;
      default:
        return 'Ok';
        break;
    }
  }

  String favoritesStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Favorites';
        break;
      case LanguageEnum.es:
        return 'Favoritos';
        break;
      default:
        return 'Favorites';
        break;
    }
  }

  String seeAllStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'See All';
        break;
      case LanguageEnum.es:
        return 'Ver Todos';
        break;
      default:
        return 'See All';
        break;
    }
  }

  String seeDescriptionStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'See description';
        break;
      case LanguageEnum.es:
        return 'Ver descripción';
        break;
      default:
        return 'See description';
        break;
    }
  }

  String noDescriptionStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'No description found';
        break;
      case LanguageEnum.es:
        return 'No hay descripción';
        break;
      default:
        return 'No description found';
        break;
    }
  }

  String alcoholicStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Alcoholic';
        break;
      case LanguageEnum.es:
        return 'Alcohol';
        break;
      default:
        return 'Alcoholic';
        break;
    }
  }

  String alcoholicsStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Alcoholic';
        break;
      case LanguageEnum.es:
        return 'Alcohol';
        break;
      default:
        return 'Alcoholic';
        break;
    }
  }

  String categoryStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Category';
        break;
      case LanguageEnum.es:
        return 'Categoría';
        break;
      default:
        return 'Category';
        break;
    }
  }

  String categoriesStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Categories';
        break;
      case LanguageEnum.es:
        return 'Categorías';
        break;
      default:
        return 'Categories';
        break;
    }
  }

  String glassStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Glass';
        break;
      case LanguageEnum.es:
        return 'Copa';
        break;
      default:
        return 'Glass';
        break;
    }
  }

  String glassesStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Glasses';
        break;
      case LanguageEnum.es:
        return 'Copas';
        break;
      default:
        return 'Glasses';
        break;
    }
  }

  String ingredientStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Ingredient';
        break;
      case LanguageEnum.es:
        return 'Ingrediente';
        break;
      default:
        return 'Ingredient';
        break;
    }
  }

  String drinkTypeStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Drink';
        break;
      case LanguageEnum.es:
        return 'Trago';
        break;
      default:
        return 'Drink';
        break;
    }
  }

  String ingredientsStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Ingredients';
        break;
      case LanguageEnum.es:
        return 'Ingredientes';
        break;
      default:
        return 'Ingredients';
        break;
    }
  }

  String allAlcoholicsStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'All alcoholic';
        break;
      case LanguageEnum.es:
        return 'Alcoholes';
        break;
      default:
        return 'All alcoholic';
        break;
    }
  }

  String allCategoriesStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'All categories';
        break;
      case LanguageEnum.es:
        return 'Todas las categorías';
        break;
      default:
        return 'All categories';
        break;
    }
  }

  String allGlassesStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'All glasses';
        break;
      case LanguageEnum.es:
        return 'Todas las copas';
        break;
      default:
        return 'All glasses';
        break;
    }
  }

  String allIngredientsStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'All ingredients';
        break;
      case LanguageEnum.es:
        return 'Todos los ingredientes';
        break;
      default:
        return 'All ingredients';
        break;
    }
  }

  String noDataStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'No data found';
        break;
      case LanguageEnum.es:
        return 'No hay datos';
        break;
      default:
        return 'No data found';
        break;
    }
  }

  String instructionsStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Instructions';
        break;
      case LanguageEnum.es:
        return 'Instrucciones';
        break;
      default:
        return 'Instructions';
        break;
    }
  }

  String removedFromFavorites() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Removed from favorites';
        break;
      case LanguageEnum.es:
        return 'Eliminado de favoritos';
        break;
      default:
        return 'Removed from favorites';
        break;
    }
  }

  String addedToFavorites() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Added to favorites';
        break;
      case LanguageEnum.es:
        return 'Agregado a favoritos';
        break;
      default:
        return 'Added to favorites';
        break;
    }
  }

  String alcoholicBeverageStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Alcoholic beverage';
        break;
      case LanguageEnum.es:
        return 'Bebida alcohólica';
        break;
      default:
        return 'Alcoholic beverage';
        break;
    }
  }

  String nonAlcoholicBeverageStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Soft drink';
        break;
      case LanguageEnum.es:
        return 'Bebida sin alcohol';
        break;
      default:
        return 'Soft drink';
        break;
    }
  }

  String recentSearchesStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Recent searches';
        break;
      case LanguageEnum.es:
        return 'Búsquedas recientes';
        break;
      default:
        return 'Recent searches';
        break;
    }
  }

  String dontKnowWhatToDrinkStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return "Don't know what to drink?";
        break;
      case LanguageEnum.es:
        return '¿No sabes qué beber?';
        break;
      default:
        return "Don't know what to drink?";
        break;
    }
  }

  String tryWithRandomStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Try with random search!';
        break;
      case LanguageEnum.es:
        return 'Prueba búsqueda aleatoria';
        break;
      default:
        return 'Try with random search!';
        break;
    }
  }

  String randomSearchStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Random search';
        break;
      case LanguageEnum.es:
        return 'Búsqueda aleatoria';
        break;
      default:
        return 'Random search';
        break;
    }
  }

  String appInfoStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'App info';
        break;
      case LanguageEnum.es:
        return 'Información de la aplicación';
        break;
      default:
        return 'App info';
        break;
    }
  }

  String versionStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Version';
        break;
      case LanguageEnum.es:
        return 'Versión';
        break;
      default:
        return 'Version';
        break;
    }
  }

  String developedByStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Developed by';
        break;
      case LanguageEnum.es:
        return 'Desarrollado por';
        break;
      default:
        return 'Developed by';
        break;
    }
  }

  String deleteStr() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Delete';
        break;
      case LanguageEnum.es:
        return 'Eliminar';
        break;
      default:
        return 'Delete';
        break;
    }
  }

  String removedFromPinnedItems() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Removed from main page';
        break;
      case LanguageEnum.es:
        return 'Eliminado de la pantalla principal';
        break;
      default:
        return 'Removed from main page';
        break;
    }
  }

  String addedToPinnedItems() {
    switch (this.language) {
      case LanguageEnum.en:
        return 'Added to main page';
        break;
      case LanguageEnum.es:
        return 'Agregado a la pantalla principal';
        break;
      default:
        return 'Added to main page';
        break;
    }
  }



}