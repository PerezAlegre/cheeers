import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/core/view_models/theme_notifier.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  LanguageEnum _languageEnumGroupValue;

  @override
  void initState() {
    // WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      final languageNotifier = Provider.of<LanguageNotifier>(context, listen: false);
      _languageEnumGroupValue = languageNotifier.language;
    // });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final themeNotifier = Provider.of<ThemeNotifier>(context, listen: true);
    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);

    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Icon(AntDesign.arrowleft),
            ),
          onTap: () => Navigator.of(context).pop(),
        ),
        title: Text(strings.settingsStr(),
          style: Theme.of(context).primaryTextTheme.subtitle2,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ListTile(
                leading: Icon(Ionicons.ios_moon,
                  color: Theme.of(context).iconTheme.color,
                ),
                title: Text(strings.darkModeStr(),
                  style: Theme.of(context).primaryTextTheme.subtitle1,
                ),
                trailing: Switch(
                  value: themeNotifier.themeMode == ThemeMode.dark ? true : false,
                  onChanged: (bool value) {
                    themeNotifier.setTheme(value ? ThemeMode.dark : ThemeMode.light);
                  }
                ),
              ),
              ListTile(
                leading: Icon(Ionicons.ios_globe,
                  color: Theme.of(context).iconTheme.color,
                ),
                title: Text(strings.languageStr(),
                  style: Theme.of(context).primaryTextTheme.subtitle1,
                ),
                subtitle: Text(languageNotifier.languageStr ?? ''),
                onTap: () {
                  showDialog(
                    context: context,
                    barrierDismissible: true,
                    builder: (context) => StatefulBuilder(
                      builder: (BuildContext context, StateSetter setState) => AlertDialog(
                        contentTextStyle: Theme.of(context).textTheme.bodyText2,
                        content: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                              RadioListTile<LanguageEnum>(
                                value: LanguageEnum.en,
                                groupValue: _languageEnumGroupValue,
                                title: Text('English'),
                                onChanged: (val) {
                                  setState(() {
                                    _languageEnumGroupValue = val;
                                  });
                                },
                                activeColor: Theme.of(context).toggleableActiveColor,
                              ),
                              RadioListTile<LanguageEnum>(
                                value: LanguageEnum.es,
                                groupValue: _languageEnumGroupValue,
                                title: Text('Español'),
                                onChanged: (val) {
                                  setState(() {
                                    _languageEnumGroupValue = val;
                                  });
                                },
                                activeColor: Theme.of(context).toggleableActiveColor,
                              ),
                            ],
                          ),
                        ),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(strings.cancelStr().toUpperCase()),
                            textColor: Theme.of(context).colorScheme.secondary,
                            onPressed: () => Navigator.of(context).pop(),
                          ),
                          FlatButton(
                            child: Text(strings.okStr().toUpperCase()),
                            textColor: Theme.of(context).colorScheme.secondary,
                            onPressed: () {
                              languageNotifier.setLanguage(_languageEnumGroupValue);
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                    )
                  );
                },
              ),
              ListTile(
                leading: Icon(Ionicons.ios_information_circle_outline,
                  color: Theme.of(context).iconTheme.color,
                ),
                title: Text(strings.appInfoStr(),
                  style: Theme.of(context).primaryTextTheme.subtitle1,
                ),
                onTap: () => Navigator.of(context).pushNamed(ConstantsRoutes.appInfoRoute),
              ),
            ],
          ),
        ),
      ),
    );
  }
}