import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/core/models/screen_arguments.dart';
import 'package:cheeers/src/core/models/search_item.dart';
import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/core/view_models/search_notifier.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchItemCard extends StatelessWidget {

  final SearchItem searchItem;
  final Widget trailing;

  SearchItemCard({
    Key key,
    @required this.searchItem,
    this.trailing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);
    final searchNotifier = Provider.of<SearchNotifier>(context, listen: false);

    final double fixedHeight = 60.0;

    String _getSearchTypeStr(SearchType searchType) {
      switch (searchType) {
        case SearchType.drink:
          return strings.drinkTypeStr();
        case SearchType.ingredient:
          return strings.ingredientStr();
        default:
          return '';
      }
    }

    return GestureDetector(
      child: Container(
        width: double.infinity,
        height: fixedHeight,
        color: Theme.of(context).primaryColor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: fixedHeight,
              width: fixedHeight,
              alignment: Alignment.center,
              child: Image.network(this.searchItem.itemImageUrl,
                alignment: Alignment.center,
                fit: BoxFit.fitHeight,
                loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent loadingProgress) {
                  if (loadingProgress == null)
                    return child;
                  return SizedBox(
                    height: 60.0,
                    width: 60.0,
                    child: Container(
                      color: Theme.of(context).colorScheme.primaryVariant,
                    )
                  );
                  // return Center(
                  //   child: CircularProgressIndicator(
                  //     value: loadingProgress.expectedTotalBytes != null
                  //         ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                  //         : null,
                  //   ),
                  // );
                },
              ),
            ),
            Expanded(
              child: Container(
                height: fixedHeight,
                padding: EdgeInsets.only(left: 16.0, top: 8.0, bottom: 8.0, right: 4.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(this.searchItem.itemName,
                      style: Theme.of(context).primaryTextTheme.bodyText1,
                      overflow: TextOverflow.fade,
                      textAlign: TextAlign.left,
                    ),
                    Text(_getSearchTypeStr(this.searchItem.searchType),
                      style: Theme.of(context).primaryTextTheme.caption,
                      overflow: TextOverflow.fade,
                      textAlign: TextAlign.left,
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: fixedHeight,
              width: fixedHeight,
              alignment: Alignment.center,
              child: Center(
                child: this.trailing,
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        if (this.searchItem.searchType == SearchType.drink) {
          searchNotifier.updateHistorySearchList(this.searchItem);
          Navigator.of(context).pushNamed(
            ConstantsRoutes.drinkDetailRoute,
            arguments: DrinkDetailArguments(
              drinkId: this.searchItem.itemId,
            )
          );
        }
        if (this.searchItem.searchType == SearchType.ingredient) {
          searchNotifier.updateHistorySearchList(this.searchItem);
          Navigator.of(context).pushNamed(
            ConstantsRoutes.ingredientDetailRoute,
            arguments: IngredientDetailArguments(
              strIngredient: this.searchItem.itemName,
            )
          );
        }
      },
    );
  }
}