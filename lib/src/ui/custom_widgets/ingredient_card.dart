import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class IngredientCard extends StatelessWidget {

  final String imageUrl;
  final String ingredientStr;
  final String seeDescriptionText;
  final Function onTap;

  IngredientCard({
    Key key,
    @required this.imageUrl,
    this.ingredientStr,
    @required this.seeDescriptionText,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4.0),
      clipBehavior: Clip.antiAlias,
      child: Container(
        height: 100.0,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Theme.of(context).colorScheme.primaryVariant,
              Theme.of(context).colorScheme.primary,
            ],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            tileMode: TileMode.clamp,
          ),
        ),
        child: GestureDetector(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.center,
                  child: Hero(
                    tag: this.ingredientStr,
                    child: Image.network(this.imageUrl,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                )
              ),
              Expanded(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.only(right: 8.0),
                  alignment: Alignment.centerRight,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(this.seeDescriptionText,
                        style: Theme.of(context).primaryTextTheme.bodyText1,
                      ),
                      SizedBox(
                        width: 4.0
                      ),
                      Icon(AntDesign.arrowright,
                        color: Theme.of(context).primaryTextTheme.bodyText1.color,
                        size: Theme.of(context).primaryTextTheme.bodyText1.fontSize,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          onTap: this.onTap,
        ),
      )
    );
  }
}