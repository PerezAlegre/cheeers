import 'package:flutter/material.dart';

class DrinkFilterCard extends StatelessWidget {

  final String idDrink;
  final String drinkName;
  final String imagePreviewUrl;
  final Color color;
  final Function onTap;

  DrinkFilterCard({
    Key key,
    @required this.idDrink,
    @required this.drinkName,
    @required this.imagePreviewUrl,
    this.color,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    final deviceSize = MediaQuery.of(context).size;
    final double totalWidth  = deviceSize.width / 3 - 20;

    return ClipRRect(
      borderRadius: BorderRadius.circular(4.0),
      clipBehavior: Clip.antiAlias,
      child: GestureDetector(
        child: SizedBox(
          width: totalWidth,
          child: Column(
            children: <Widget>[
              Flexible(
                flex: 2,
                child: Hero(
                  tag: this.idDrink,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(this.imagePreviewUrl),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            this.color.withOpacity(0.1),
                            this.color.withOpacity(0.2),
                          ],
                          begin: Alignment.center,
                          end: Alignment.bottomCenter,
                        )
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        this.color.withOpacity(0.8),
                        this.color,
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter
                    ),
                  ),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 8.0),
                      child: Text(this.drinkName,
                        maxLines: 4,
                        overflow: TextOverflow.fade,
                        style: Theme.of(context).primaryTextTheme.bodyText1.copyWith(color: Theme.of(context).primaryColor),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        onTap: this.onTap,
      ),
    );
  }

}