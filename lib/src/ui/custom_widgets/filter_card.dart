import 'package:flutter/material.dart';

class FilterCard extends StatelessWidget {

  final String title;
  final Color color;
  final Function onTap;
  final Function onLongPress;
  final String searchValue;

  FilterCard({
    Key key,
    @required this.title,
    this.color,
    this.onTap,
    this.onLongPress,
    this.searchValue,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4.0),
      clipBehavior: Clip.antiAlias,
      child: GestureDetector(
        child: Container(
          height: 60.0,
          width: 120.0,
          //color: this.color,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                this.color.withOpacity(0.7),
                this.color,
              ],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              tileMode: TileMode.clamp,
            )
          ),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.only(left: 8.0),
              child: Text(this.title,
                maxLines: 4,
                overflow: TextOverflow.fade,
                style: Theme.of(context).primaryTextTheme.bodyText1.copyWith(color: Theme.of(context).primaryColor),
              ),
            ),
          ),
        ),
        onTap: this.onTap,
        onLongPress: this.onLongPress,
      ),
    );
  }
}