import 'package:flutter/material.dart';

class GradientIcon extends StatelessWidget {

  final Widget child;

  GradientIcon({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      shaderCallback: (Rect rect) {
        return RadialGradient(
          colors: [
            Theme.of(context).colorScheme.secondary,
            Theme.of(context).colorScheme.secondaryVariant,
          ],
          center: Alignment.topCenter,
          radius: 1.0,
          tileMode: TileMode.clamp,
        ).createShader(Rect.fromLTRB(0, 0, rect.width, rect.height));
      },
      blendMode: BlendMode.srcIn,
      child: this.child,
    );
  }
}