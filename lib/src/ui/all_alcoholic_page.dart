import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/core/models/screen_arguments.dart';
import 'package:cheeers/src/core/view_models/alcoholic_list_notifier.dart';
import 'package:cheeers/src/core/view_models/drinks_filter_list_notifier.dart';
import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/ui/custom_widgets/filter_card.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class AllAlcoholicPage extends StatefulWidget {
  @override
  _AllAlcoholicPageState createState() => _AllAlcoholicPageState();
}

class _AllAlcoholicPageState extends State<AllAlcoholicPage> {

  bool _showSearchBar;
  String _searchValue;
  final _searchController = TextEditingController();

  @override
  void initState() {
    _showSearchBar = false;
    _searchValue = '';
    _searchController.addListener(_searchValueText);
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void _searchValueText() {
    setState(() {
      _searchValue = _searchController.text;
    });
  }

  void _cleanSearchBar() {
    _searchController.clear();
  }

  void _toggleSearchBar() {
    _cleanSearchBar();
    setState(() {
      _showSearchBar = !_showSearchBar;
    });
  }

  @override
  Widget build(BuildContext context) {

    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);
    final drinksFilterListNotifier = Provider.of<DrinksFilterListNotifier>(context, listen: false);

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          
          _showSearchBar
          ? SliverAppBar(
            elevation: 0.0,
            leading: GestureDetector(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Icon(AntDesign.arrowleft),
              ),
              onTap: _toggleSearchBar,
            ),
            automaticallyImplyLeading: false,
            title: Container(
              width: double.infinity,
              child: TextField(
                controller: _searchController,
                textInputAction: TextInputAction.search,
                style: TextStyle(fontSize: 18.0),
                decoration: InputDecoration(
                  enabledBorder: InputBorder.none,
                  hintText: '${strings.searchStr()} ${strings.alcoholicStr().toLowerCase()}',
                  fillColor: Colors.transparent,
                  focusColor: Colors.transparent,
                  suffixIcon: _searchValue.length > 0 ? Padding(
                    padding: const EdgeInsetsDirectional.only(end: 0.0),
                    child: GestureDetector(
                      child: Container(
                        alignment: Alignment.centerRight,
                        width: 48.0,
                        height: 48.0,
                        child: Icon(AntDesign.close,
                          color: Theme.of(context).primaryIconTheme.color,
                        ),
                      ),
                      onTap: _cleanSearchBar,
                    ),
                  )
                  : Padding(
                    padding: const EdgeInsetsDirectional.only(end: 0.0),
                    child: SizedBox(
                      width: 0.0,
                      height: 48.0
                    ),
                  ),
                ),
                autofocus: true,
              ),
            ),
            floating: false,
            pinned: true,
            snap: false,
          )
          : SliverAppBar(
            elevation: 0.0,
            leading: GestureDetector(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Icon(AntDesign.arrowleft),
                ),
              onTap: () => Navigator.of(context).pop(),
            ),
            flexibleSpace: FlexibleSpaceBar(
              title: Text(strings.allAlcoholicsStr(),
                style: Theme.of(context).primaryTextTheme.subtitle2,
              ),
              titlePadding: EdgeInsetsDirectional.only(start: 0.0, bottom: 16.0),
              centerTitle: true,
              collapseMode: CollapseMode.parallax,
              background: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Theme.of(context).colorScheme.primaryVariant,
                      Theme.of(context).colorScheme.primary,
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    tileMode: TileMode.clamp,
                  ),
                ),
              ),
            ),
            expandedHeight: 140.0,
            floating: false,
            pinned: true,
            snap: false,
            actions: <Widget>[
              GestureDetector(
                child: Padding(
                  padding: EdgeInsets.only(right: 16.0),
                  child: Icon(AntDesign.search1),
                ),
                onTap: _toggleSearchBar,
              ),
            ]
          ),

          Consumer<AlcoholicListNotifier>(
            builder: (context, alcoholicsList, _) {
              var alcoholicsListFilter = _searchValue.length > 0
              ? alcoholicsList?.alcoholicsList?.where((element) => element.strAlcoholic.toLowerCase().contains(_searchValue.toLowerCase()))?.toList()
              : alcoholicsList.alcoholicsList;
              if (alcoholicsListFilter != null && alcoholicsListFilter.length > 0) {
                return SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 12.0,
                    mainAxisSpacing: 12.0,
                  ),
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return FilterCard(
                        title: alcoholicsListFilter[index].strAlcoholic,
                        color: Theme.of(context).colorScheme.secondary,
                        onTap: () {
                          drinksFilterListNotifier.getAllDrinksList(
                            filterType: FilterType.alcoholic,
                            filterValue: alcoholicsListFilter[index].strAlcoholic
                          );
                          Navigator.of(context).pushNamed(
                            ConstantsRoutes.drinksFilterRoute,
                            arguments: DrinksFilterListArguments(
                              filterType: FilterType.alcoholic,
                              drinkFilter: alcoholicsListFilter[index].strAlcoholic
                            )
                          );
                        },
                      );
                    },
                    childCount: alcoholicsListFilter.length,
                  ),
                );
              } else {
                return SliverFillRemaining(
                  child: Center(
                    child: Text(strings.noDataStr())
                  ),
                );
              }
            },
          ),

          SliverToBoxAdapter(
            child: SizedBox(
              height: 40.0,
            ),
          ),


        ],
      ),
    );
  }
}