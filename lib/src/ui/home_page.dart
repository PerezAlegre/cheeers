import 'package:cheeers/src/ui/custom_widgets/gradient_icon.dart';
import 'package:cheeers/src/ui/explore_page.dart';
import 'package:cheeers/src/ui/favorites_page.dart';
import 'package:cheeers/src/ui/search_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  int _selectedPage = 0;

  List<Widget> _tabPageOptions = [
    ExplorePage(key: PageStorageKey('ExplorePage')),
    SearchPage(key: PageStorageKey('SearchPage')),
    FavoritesPage(key: PageStorageKey('FavoritesPage')),
  ];

  final PageStorageBucket _bucket = PageStorageBucket();

  List _visitedPages;

  @override
  void initState() {
    _visitedPages = new List<int>()..add(_selectedPage);
    super.initState();
  }

  void _goForwardVisitedPages(int index) {
    List<int> visitedPagesList = _visitedPages;
    if (visitedPagesList.contains(index)) {
      visitedPagesList.remove(index);
      visitedPagesList.add(index);
    } else {
      visitedPagesList.add(index);
    }
    setState(() {
      _visitedPages = visitedPagesList;
      _selectedPage = _visitedPages[_visitedPages.length - 1];
    });
  }

  void _goBackVisitedPages() {
    setState(() {
      _visitedPages.removeLast();
      _selectedPage = _visitedPages.length > 0 ? _visitedPages[_visitedPages.length - 1] : _selectedPage;
    });
  }

  Future<bool> _onWillPop() {
    _goBackVisitedPages();
    return new Future<bool>(() {
      if (_visitedPages.length < 1) {
        return true;
      }
      return false;
    });
  }

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: PageStorage(
          child: _tabPageOptions[_selectedPage],
          bucket: _bucket,
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Theme.of(context).colorScheme.primaryVariant,
          currentIndex: _selectedPage,
          type: BottomNavigationBarType.fixed,
          onTap: (int index) {
            setState(() {
              _goForwardVisitedPages(index);
            });
          },
          // selectedItemColor: Theme.of(context).colorScheme.secondary,
          // unselectedItemColor: Colors.grey,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          iconSize: 28.0,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              activeIcon: GradientIcon(child: Icon(AntDesign.rocket1)),
              icon: Icon(AntDesign.rocket1, color: Colors.grey),
              title: Container(),
              backgroundColor: Theme.of(context).colorScheme.secondary,
            ),
            BottomNavigationBarItem(
              activeIcon: GradientIcon(child: Icon(AntDesign.search1)),
              icon: Icon(AntDesign.search1, color: Colors.grey),
              title: Container(),
              backgroundColor: Theme.of(context).colorScheme.secondary,
            ),
            BottomNavigationBarItem(
              activeIcon: GradientIcon(child: Icon(AntDesign.heart)),
              icon: Icon(AntDesign.heart, color: Colors.grey),
              title: Container(),
              backgroundColor: Theme.of(context).colorScheme.secondary,
            ),
          ],
        ),
      ),
    );
  }
}