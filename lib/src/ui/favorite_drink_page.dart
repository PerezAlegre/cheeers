import 'package:cheeers/src/core/models/screen_arguments.dart';
import 'package:cheeers/src/core/view_models/favorites_list_notifier.dart';
import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class FavoriteDrinkPage extends StatefulWidget {

  final int initialPage;

  FavoriteDrinkPage({
    Key key,
    @required this.initialPage,
  }) : super(key: key);

  @override
  _FavoriteDrinkPageState createState() => _FavoriteDrinkPageState();
}

class _FavoriteDrinkPageState extends State<FavoriteDrinkPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _showImage;
  PageController _pageController;

  @override
  void initState() {
    _showImage = false;
    _pageController = new PageController(
      initialPage: widget.initialPage
    );
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);
    final favoritesListNotifier = Provider.of<FavoritesListNotifier>(context, listen: true);

    final double safeHeight = MediaQuery.of(context).padding.top;
    final double safeHeightForIcons = safeHeight + 16.0;

    Widget _ingredientAndMeasure({@required String ingredient, @required String measure}) {
      return ingredient != null && ingredient.isNotEmpty
        ? Container(
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 2.0),
          width: MediaQuery.of(context).size.width - 32.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                child: Text(ingredient,
                  maxLines: 100,
                  style: Theme.of(context).primaryTextTheme.bodyText2.copyWith(
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w500
                  ),
                ),
                onTap: () => Navigator.of(context).pushNamed(
                  ConstantsRoutes.ingredientDetailRoute,
                  arguments: IngredientDetailArguments(
                    strIngredient: ingredient,
                  )
                ),
              ),
              Text(measure != null ? ':  $measure' : '',
                maxLines: 100,
                style: Theme.of(context).primaryTextTheme.bodyText2,
              )
            ],
          ),
        )
        : Container();
    }

    return Consumer<FavoritesListNotifier>(
      builder: (context, favorites, _) {
        if (favorites.favoritesList != null && favorites.favoritesList.length > 0) {
          return Scaffold(
            key: _scaffoldKey,
            backgroundColor: Theme.of(context).primaryColor,
            body: PageView.builder(
              controller: _pageController,
              scrollDirection: Axis.horizontal,
              physics: BouncingScrollPhysics(),
              itemCount: favorites.favoritesList.length,
              itemBuilder: (context, index) => Stack(
                children: _showImage
                ? <Widget>[
                  Hero(
                    tag: favorites.favoritesList[index].idDrink,
                    child: GestureDetector(
                      child: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(favorites.favoritesList[index].strDrinkThumb),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          _showImage = false;
                        });
                      },
                    ),
                  ),
                ]
                : <Widget>[
                  Hero(
                    tag: favorites.favoritesList[index].idDrink,
                    child: GestureDetector(
                      child: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(favorites.favoritesList[index].strDrinkThumb),
                            fit: BoxFit.cover,
                          ),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Theme.of(context).primaryColor.withOpacity(1.0),
                                Theme.of(context).primaryColor.withOpacity(0.6),
                                Theme.of(context).primaryColor.withOpacity(0.1),
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                              tileMode: TileMode.clamp,
                            ),
                          ),
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          _showImage = true;
                        });
                      },
                    ),
                  ),
                  Positioned(
                    top: safeHeightForIcons,
                    left: 16.0,
                    child: GestureDetector(
                      child: ClipOval(
                        clipBehavior: Clip.antiAlias,
                        child: Container(
                          width: 40.0,
                          height: 40.0,
                          color: Theme.of(context).primaryColor.withOpacity(0.5),
                          child: Icon(AntDesign.arrowleft)
                        ),
                      ),
                      onTap: () => Navigator.of(context).pop(),
                    ),
                  ),
                  Positioned(
                    top: safeHeightForIcons,
                    right: 16.0,
                    child: GestureDetector(
                      child: ClipOval(
                        clipBehavior: Clip.antiAlias,
                        child: Container(
                          width: 40.0,
                          height: 40.0,
                          color: Theme.of(context).primaryColor.withOpacity(0.5),
                          child: Consumer<FavoritesListNotifier>(
                            builder: (context, favorites, _) {
                              if (favorites.favoritesList.toList().where((value) => value.idDrink == favorites.favoritesList[index].idDrink).length == 1) {
                                return Icon(AntDesign.heart,
                                  color: Theme.of(context).colorScheme.secondary
                                );
                              } else {
                                return Icon(AntDesign.hearto,
                                  color: Theme.of(context).colorScheme.secondary
                                );
                              }
                            }
                          ),
                        ),
                      ),
                      onTap: () {
                        if (favoritesListNotifier.favoritesList.toList().where((value) => value.idDrink == favorites.favoritesList[index].idDrink).length == 1) {
                          favoritesListNotifier.removeFavorite(favorites.favoritesList[index]);
                          _scaffoldKey.currentState.removeCurrentSnackBar();
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            duration: Duration(seconds: 3),
                            content: Text(strings.removedFromFavorites()),
                          ));
                        } else {
                          favoritesListNotifier.addFavorite(favorites.favoritesList[index]);
                          _scaffoldKey.currentState.removeCurrentSnackBar();
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            duration: Duration(seconds: 3),
                            content: Text(strings.addedToFavorites()),
                          ));
                        }
                      },
                    ),
                  ),
                  Positioned(
                    top: MediaQuery.of(context).size.height * 0.5,
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.5,
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 16.0),
                              width: MediaQuery.of(context).size.width - 32.0,
                              child: Text(favorites.favoritesList[index].strDrink,
                                style: Theme.of(context).primaryTextTheme.headline6,
                                maxLines: 3,
                                textAlign: TextAlign.left,
                              ),
                            ),

                            Container(
                              height: 60.0,
                              width: MediaQuery.of(context).size.width,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                physics: BouncingScrollPhysics(),
                                children: <Widget>[
                                  favorites.favoritesList[index].strCategory != null 
                                    ? Padding(
                                      padding: EdgeInsets.only(left: 16.0),
                                      child: Chip(
                                        label: Text(favorites.favoritesList[index].strCategory,
                                          style: Theme.of(context).primaryTextTheme.bodyText2,
                                        ),
                                        backgroundColor: Theme.of(context).primaryColor.withOpacity(0.8),
                                      ),
                                    )
                                    : Container(),
                                  favorites.favoritesList[index].strGlass != null 
                                    ? Padding(
                                      padding: EdgeInsets.only(left: 16.0),
                                      child: Chip(
                                        label: Text(favorites.favoritesList[index].strGlass,
                                          style: Theme.of(context).primaryTextTheme.bodyText2,
                                        ),
                                        backgroundColor: Theme.of(context).primaryColor.withOpacity(0.8),
                                      ),
                                    )
                                    : Container(),
                                  favorites.favoritesList[index].strAlcoholic != null 
                                    ? Padding(
                                      padding: EdgeInsets.only(left: 16.0, right: 16.0),
                                      child: Chip(
                                        label: Text(favorites.favoritesList[index].strAlcoholic,
                                          style: Theme.of(context).primaryTextTheme.bodyText2,
                                        ),
                                        backgroundColor: Theme.of(context).primaryColor.withOpacity(0.8),
                                      ),
                                    )
                                    : Container(),
                                ],
                              ),
                            ),
                            
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.0),
                              child: Text(strings.ingredientsStr(),
                                style: Theme.of(context).primaryTextTheme.subtitle2,
                                textAlign: TextAlign.left,
                              ),
                            ),
                            
                            SizedBox(
                              height: 5.0,
                            ),
                            
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient1,
                              measure: favorites.favoritesList[index].strMeasure1,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient2,
                              measure: favorites.favoritesList[index].strMeasure2,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient3,
                              measure: favorites.favoritesList[index].strMeasure3,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient4,
                              measure: favorites.favoritesList[index].strMeasure4,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient5,
                              measure: favorites.favoritesList[index].strMeasure5,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient6,
                              measure: favorites.favoritesList[index].strMeasure6,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient7,
                              measure: favorites.favoritesList[index].strMeasure7,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient8,
                              measure: favorites.favoritesList[index].strMeasure8,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient9,
                              measure: favorites.favoritesList[index].strMeasure9,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient10,
                              measure: favorites.favoritesList[index].strMeasure10,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient11,
                              measure: favorites.favoritesList[index].strMeasure11,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient12,
                              measure: favorites.favoritesList[index].strMeasure12,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient13,
                              measure: favorites.favoritesList[index].strMeasure13,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient14,
                              measure: favorites.favoritesList[index].strMeasure14,
                            ),
                            _ingredientAndMeasure(
                              ingredient: favorites.favoritesList[index].strIngredient15,
                              measure: favorites.favoritesList[index].strMeasure15,
                            ),

                            SizedBox(
                              height: 10.0,
                            ),
                            
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 16.0),
                              child: Text(strings.instructionsStr(),
                                style: Theme.of(context).primaryTextTheme.subtitle2,
                                textAlign: TextAlign.left,
                              ),
                            ),
                            
                            SizedBox(
                              height: 5.0,
                            ),
                            
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 16.0),
                              width: MediaQuery.of(context).size.width - 32.0,
                              child: Text(favorites.favoritesList[index].strInstructions,
                                maxLines: 100,
                                style: Theme.of(context).primaryTextTheme.bodyText2,
                              ),
                            ),
                            
                            SizedBox(
                              height: 16.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        } else {
          return Scaffold(
            backgroundColor: Theme.of(context).primaryColor,
            appBar: AppBar(
              leading: GestureDetector(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Icon(AntDesign.arrowleft),
                  ),
                onTap: () => Navigator.of(context).pop(),
              ),
            ),
            body: Center(
              child: Text(strings.noDataStr())
            ),
          );
        }
      },
    );
  }
}