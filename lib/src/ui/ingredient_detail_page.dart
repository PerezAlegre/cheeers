import 'package:cheeers/src/core/services/api_service.dart';
import 'package:cheeers/src/core/view_models/ingredient_provider.dart';
import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class IngredientDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);

    final double safeHeight = MediaQuery.of(context).padding.top;
    final double safeHeightForIcons = safeHeight + 16.0;
    
    return Consumer<IngredientProvider>(
      builder: (context, ingredientProvider, _) {
        if (ingredientProvider.ingredient != null) {
          return Scaffold(
            backgroundColor: Theme.of(context).primaryColor,
            body: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Theme.of(context).colorScheme.primaryVariant,
                        Theme.of(context).colorScheme.primary,
                      ],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      tileMode: TileMode.clamp,
                    ),
                  ),
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.4,
                  alignment: Alignment.center,
                  child: ShaderMask(
                    shaderCallback: (rect) {
                      return LinearGradient(
                        colors: [
                          Colors.black,
                          Colors.black,
                          Theme.of(context).primaryColor.withOpacity(0.3),
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        tileMode: TileMode.clamp
                      ).createShader(rect);
                    },
                    blendMode: BlendMode.dstIn,
                    child: Hero(
                      tag: ingredientProvider.ingredient.strIngredient,
                      child: Image.network(ApiService.INGREDIENT_THUMBNAIL_LARGE_URL.replaceFirst('{0}', ingredientProvider.ingredient.strIngredient),
                        fit: BoxFit.fitHeight,
                      ),
                    )
                  ),
                ),
                Positioned(
                  top: safeHeightForIcons,
                  left: 16.0,
                  child: GestureDetector(
                    child: ClipOval(
                      clipBehavior: Clip.antiAlias,
                      child: Container(
                        width: 40.0,
                        height: 40.0,
                        color: Theme.of(context).primaryColor.withOpacity(0.5),
                        child: Icon(AntDesign.arrowleft)
                      ),
                    ),
                    onTap: () => Navigator.of(context).pop(),
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).size.height * 0.4,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.6,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 16.0),
                            width: MediaQuery.of(context).size.width - 32.0,
                            child: Text(ingredientProvider.ingredient.strIngredient,
                              style: Theme.of(context).primaryTextTheme.headline6,
                              maxLines: 3,
                              textAlign: TextAlign.left,
                            ),
                          ),

                          Container(
                            height: 60.0,
                            width: MediaQuery.of(context).size.width,
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              physics: BouncingScrollPhysics(),
                              children: <Widget>[
                                ingredientProvider.ingredient.strType != null 
                                  ? Padding(
                                    padding: EdgeInsets.only(left: 16.0),
                                    child: Chip(
                                      label: Text(ingredientProvider.ingredient.strType,
                                        style: Theme.of(context).primaryTextTheme.bodyText2,
                                      ),
                                      backgroundColor: Theme.of(context).primaryColor.withOpacity(0.8),
                                    ),
                                  )
                                  : Container(),
                                ingredientProvider.ingredient.strAlcohol != null 
                                  ? Padding(
                                    padding: EdgeInsets.only(left: 16.0),
                                    child: Chip(
                                      label: Text(ingredientProvider.ingredient.strAlcohol.toLowerCase() == 'yes' ? strings.alcoholicBeverageStr() : strings.nonAlcoholicBeverageStr(),
                                        style: Theme.of(context).primaryTextTheme.bodyText2,
                                      ),
                                      backgroundColor: Theme.of(context).primaryColor.withOpacity(0.8),
                                    ),
                                  )
                                  : Container(),
                                ingredientProvider.ingredient.strABV != null 
                                  ? Padding(
                                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                                    child: Chip(
                                      label: Text('ABV: ${ingredientProvider.ingredient.strABV}%',
                                        style: Theme.of(context).primaryTextTheme.bodyText2,
                                      ),
                                      backgroundColor: Theme.of(context).primaryColor.withOpacity(0.8),
                                    ),
                                  )
                                  : Container(),
                              ],
                            ),
                          ),

                          SizedBox(
                            height: 5.0,
                          ),

                          ingredientProvider.ingredient.strDescription != null
                            ? Container(
                                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                                width: MediaQuery.of(context).size.width - 32.0,
                                child: Text(ingredientProvider.ingredient.strDescription,
                                  maxLines: 500,
                                  style: Theme.of(context).primaryTextTheme.bodyText2,
                                  textAlign: TextAlign.left,
                                ),
                              )
                            : Container(
                                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
                                width: MediaQuery.of(context).size.width - 32.0,
                                alignment: Alignment.center,
                                child: Text(strings.noDescriptionStr(),
                                  style: Theme.of(context).primaryTextTheme.bodyText2,
                                  textAlign: TextAlign.center,
                                ),
                              ),

                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        } else {
          return Scaffold(
            backgroundColor: Theme.of(context).primaryColor,
            appBar: AppBar(
              leading: GestureDetector(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Icon(AntDesign.arrowleft),
                  ),
                onTap: () => Navigator.of(context).pop(),
              ),
            ),
          );
        }
      },
    );
  }
}