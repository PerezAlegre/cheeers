import 'package:cheeers/src/core/models/search_item.dart';
import 'package:cheeers/src/core/models/theme.dart';
import 'package:cheeers/src/core/view_models/search_notifier.dart';
import 'package:cheeers/src/core/view_models/theme_notifier.dart';
import 'package:cheeers/src/ui/custom_widgets/search_item_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class SearchItems extends SearchDelegate {

  final String searchLabel;

  SearchItems({
    @required this.searchLabel,
  });

  @override
  String get searchFieldLabel => this.searchLabel;

  @override
  ThemeData appBarTheme(BuildContext context) {
    /// En el archivo search.dart: line: 509 (metodo build()) Encapsular el title en un [Theme]
    /// title: Theme(
    ///   data: theme,
    ///   child: TextField()
    /// )
    final themeNotifier = Provider.of<ThemeNotifier>(context, listen: true);
    if (themeNotifier.themeMode == ThemeMode.dark) {
      return AppTheme.darkTheme;
    } else {
      return AppTheme.lightTheme;
    }
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return query.isNotEmpty ? [
      IconButton(
        icon: Icon(AntDesign.close,
          color: Theme.of(context).primaryIconTheme.color,
        ),
        onPressed: () {
          query = '';
          buildSuggestions(context);
        },
      )
    ] : [];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Icon(AntDesign.arrowleft),
        ),
      onTap: () => Navigator.of(context).pop(),
    );
  }

  @override
  Widget buildResults(BuildContext context) {

    final searchNotifier = Provider.of<SearchNotifier>(context, listen: true);
    if (query.length > 0) {
      searchNotifier.searchItems(query);
    }

    return Consumer<SearchNotifier>(
      builder: (context, search, _) {
        if (query.length > 0) {
          return _buildResultsList(search.searchItemList);
        } else {
          return _buildResultsList(search.historySearchList);
        }
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {

    final searchNotifier = Provider.of<SearchNotifier>(context, listen: false);
    if (query.length > 0) {
      searchNotifier.searchItems(query);
    }

    return Consumer<SearchNotifier>(
      builder: (context, search, _) {
        if (query.length > 0) {
          return _buildResultsList(search.searchItemList);
        } else {
          return _buildResultsList(search.historySearchList,
            trailing: Icon(Icons.history,
              color: Colors.grey,
            )
          );
        }
      },
    );
  }

  @override
  void showResults(BuildContext context) {
    super.showResults(context);
  }

  @override
  void showSuggestions(BuildContext context) {
    super.showSuggestions(context);
  }

  Widget _buildResultsList(List<SearchItem> searchItemList, {Widget trailing}) {
    if (searchItemList != null) {
      return SingleChildScrollView(
        child: Column(
          children: searchItemList.map((searchItem) => SearchItemCard(
            searchItem: searchItem,
            trailing: trailing ?? Container()
          )).toList(),
        ),
      );
    }
    return Container();
  }

}