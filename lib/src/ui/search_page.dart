import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/core/models/screen_arguments.dart';
import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/core/view_models/search_notifier.dart';
import 'package:cheeers/src/ui/custom_widgets/drink_filter_card.dart';
import 'package:cheeers/src/ui/search_items_page.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class SearchPage extends StatefulWidget {
  
  SearchPage({Key key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {

  @override
  Widget build(BuildContext context) {
  
    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0.0,
            automaticallyImplyLeading: true,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(strings.searchStr(),
                style: Theme.of(context).primaryTextTheme.headline6,
              ),
              titlePadding: EdgeInsetsDirectional.only(start: 16.0, bottom: 56.0),
              centerTitle: false,
              collapseMode: CollapseMode.parallax,
              background: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Theme.of(context).colorScheme.primaryVariant,
                      Theme.of(context).colorScheme.primary,
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    tileMode: TileMode.clamp,
                  ),
                ),
              ),
            ),
            expandedHeight: 188.0,
            floating: false,
            pinned: true,
            snap: false,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(56.0),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
                child: Container(
                  height: 48.0,
                  width: double.infinity,
                  child: GestureDetector(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(4.0),
                      clipBehavior: Clip.antiAlias,
                      child: Container(
                        color: Theme.of(context).colorScheme.primaryVariant,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(strings.hintSearchStr(),
                              style: Theme.of(context).primaryTextTheme.bodyText2,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              width: 4.0,
                            ),
                            Icon(AntDesign.search1,
                              color: Theme.of(context).primaryTextTheme.bodyText2.color,
                              size: Theme.of(context).primaryTextTheme.bodyText1.fontSize,
                            )
                          ],
                        ),
                      )
                    ),
                    onTap: () => showSearch(
                      context: context,
                      delegate: SearchItems(
                        searchLabel: strings.hintSearchStr(),
                      ),
                    ),
                  ),
                )
              ),
            ),
          ),

          SliverToBoxAdapter(
            child: Consumer<SearchNotifier>(
              builder: (context, search, _) {
                if (search.historySearchList != null && search.historySearchList.length > 0) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Text(strings.recentSearchesStr(),
                          style: Theme.of(context).primaryTextTheme.subtitle2,
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        height: 200.0,
                        child: ListView.builder(
                          key: PageStorageKey('RecentSearchesList'),
                          scrollDirection: Axis.horizontal,
                          physics: BouncingScrollPhysics(),
                          itemCount: search.historySearchList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: index == search.historySearchList.length - 1
                                ? EdgeInsets.only(right: 16.0)
                                : index == 0
                                  ? EdgeInsets.only(left: 16.0, right: 8.0)
                                  : EdgeInsets.only(right: 8.0),
                              child: AspectRatio(
                                aspectRatio: 0.7,
                                child: DrinkFilterCard(
                                  idDrink: search.historySearchList[index].searchType == SearchType.drink
                                    ? search.historySearchList[index].itemId
                                    : search.historySearchList[index].itemName, // para el Hero tag 
                                  drinkName: search.historySearchList[index].itemName,
                                  imagePreviewUrl: search.historySearchList[index].itemImageUrl,
                                  color: Theme.of(context).colorScheme.onSurface,
                                  onTap: () {
                                    if (search.historySearchList[index].searchType == SearchType.drink) {
                                      Navigator.of(context).pushNamed(
                                        ConstantsRoutes.drinkDetailRoute,
                                        arguments: DrinkDetailArguments(
                                          drinkId: search.historySearchList[index].itemId,
                                        )
                                      );
                                    }
                                    if (search.historySearchList[index].searchType == SearchType.ingredient) {
                                      Navigator.of(context).pushNamed(
                                        ConstantsRoutes.ingredientDetailRoute,
                                        arguments: IngredientDetailArguments(
                                          strIngredient: search.historySearchList[index].itemName,
                                        )
                                      );
                                    }
                                  },
                                ),
                              ),
                            );
                          }
                        ),
                      ),
                    ],
                  );
                } else {
                  return Container();
                }
              },
            ),
          ),

          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 32.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(strings.dontKnowWhatToDrinkStr(),
                          style: Theme.of(context).primaryTextTheme.bodyText1,
                        ),
                        Text(strings.tryWithRandomStr(),
                          style: Theme.of(context).primaryTextTheme.bodyText1,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  GestureDetector(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(4.0),
                      clipBehavior: Clip.antiAlias,
                      child: Container(
                        height: 50.0,
                        width: double.infinity,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Theme.of(context).colorScheme.onSurface.withOpacity(0.8),
                              Theme.of(context).colorScheme.onSurface,
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            tileMode: TileMode.clamp,
                          ),
                        ),
                        child: Text(strings.randomSearchStr(),
                          style: Theme.of(context).primaryTextTheme.subtitle2.copyWith(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ),
                    onTap: () => Navigator.of(context).pushNamed(ConstantsRoutes.randomDrinkRoute),
                  ),
                ],
              ),
            )
          ),

          SliverToBoxAdapter(
            child: SizedBox(
              height: 40.0,
            ),
          ),


        ],
      )
    );
  }
}