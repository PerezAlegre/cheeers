import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/core/models/pinned_item.dart';
import 'package:cheeers/src/core/models/screen_arguments.dart';
import 'package:cheeers/src/core/services/api_service.dart';
import 'package:cheeers/src/core/view_models/drinks_filter_list_notifier.dart';
import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/core/view_models/pinned_items_notifier.dart';
import 'package:cheeers/src/ui/custom_widgets/drink_filter_card.dart';
import 'package:cheeers/src/ui/custom_widgets/ingredient_card.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class DrinksFilterPage extends StatefulWidget {

  final FilterType filterType;
  final String drinkFilter;

  DrinksFilterPage({
    Key key,
    @required this.filterType,
    @required this.drinkFilter,
  }) : super(key: key);

  @override
  _DrinksFilterPageState createState() => _DrinksFilterPageState();
}

class _DrinksFilterPageState extends State<DrinksFilterPage> {

  bool _showSearchBar;
  String _searchValue;
  final _searchController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _showSearchBar = false;
    _searchValue = '';
    _searchController.addListener(_searchValueText);
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  void _searchValueText() {
    setState(() {
      _searchValue = _searchController.text;
    });
  }

  void _cleanSearchBar() {
    _searchController.clear();
  }

  void _toggleSearchBar() {
    _cleanSearchBar();
    setState(() {
      _showSearchBar = !_showSearchBar;
    });
  }

  Widget _buildTitle(BuildContext context, LanguageEnum languageEnum) {
    ConstantsStrings strings = new ConstantsStrings(languageEnum);
    String filterTypeStr = '';
    switch (this.widget.filterType) {
      case FilterType.alcoholic:
        filterTypeStr = strings.alcoholicStr();
        break;
      case FilterType.category:
        filterTypeStr = strings.categoryStr();
        break;
      case FilterType.glass:
        filterTypeStr = strings.glassStr();
        break;
      case FilterType.ingredient:
        filterTypeStr = strings.ingredientStr();
        break;
      default:
        filterTypeStr = '';
        break;
    }
    return Text('$filterTypeStr: ${this.widget.drinkFilter}',
      style: Theme.of(context).primaryTextTheme.subtitle2,
      maxLines: 2,
      overflow: TextOverflow.fade,
    );
  }

  @override
  Widget build(BuildContext context) {

    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);
    final pinnedItemsNotifier = Provider.of<PinnedItemsNotifier>(context, listen: true);

    return Scaffold(
      key: _scaffoldKey,
      body: CustomScrollView(
        slivers: <Widget>[

          _showSearchBar
          ? SliverAppBar(
            elevation: 0.0,
            leading: GestureDetector(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Icon(AntDesign.arrowleft),
              ),
              onTap: _toggleSearchBar,
            ),
            automaticallyImplyLeading: false,
            title: Container(
              width: double.infinity,
              child: TextField(
                controller: _searchController,
                textInputAction: TextInputAction.search,
                style: TextStyle(fontSize: 18.0),
                decoration: InputDecoration(
                  enabledBorder: InputBorder.none,
                  hintText: '${strings.searchDrinksWithStr()} ${widget.drinkFilter}',
                  fillColor: Colors.transparent,
                  focusColor: Colors.transparent,
                  suffixIcon: _searchValue.length > 0 ? Padding(
                    padding: const EdgeInsetsDirectional.only(end: 0.0),
                    child: GestureDetector(
                      child: Container(
                        alignment: Alignment.centerRight,
                        width: 48.0,
                        height: 48.0,
                        child: Icon(AntDesign.close,
                          color: Theme.of(context).primaryIconTheme.color,
                        ),
                      ),
                      onTap: _cleanSearchBar,
                    ),
                  )
                  : Padding(
                    padding: const EdgeInsetsDirectional.only(end: 0.0),
                    child: SizedBox(
                      width: 0.0,
                      height: 48.0
                    ),
                  ),
                ),
                autofocus: true,
              ),
            ),
            floating: false,
            pinned: true,
            snap: false,
          )
          : SliverAppBar(
            elevation: 0.0,
            leading: GestureDetector(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Icon(AntDesign.arrowleft),
                ),
              onTap: () => Navigator.of(context).pop(),
            ),
            flexibleSpace: FlexibleSpaceBar(
              title: _buildTitle(context, languageNotifier.language),
              titlePadding: EdgeInsetsDirectional.only(start: 16.0, bottom: 16.0),
              centerTitle: true,
              collapseMode: CollapseMode.parallax,
              background: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Theme.of(context).colorScheme.primaryVariant,
                      Theme.of(context).colorScheme.primary,
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    tileMode: TileMode.clamp,
                  ),
                ),
              ),
            ),
            expandedHeight: 160.0,
            floating: false,
            pinned: true,
            snap: false,
            actions: <Widget>[
              GestureDetector(
                child: Padding(
                  padding: EdgeInsets.only(right: 16.0),
                  child: Icon(AntDesign.search1),
                ),
                onTap: _toggleSearchBar,
              ),
              GestureDetector(
                child: Padding(
                  padding: EdgeInsets.only(right: 16.0),
                  child: Consumer<PinnedItemsNotifier>(
                    builder: (context, pinnedItems, _) {
                      if (pinnedItems.pinnedItemList.toList().where((value) => value.filterType == this.widget.filterType && value.drinkFilter == this.widget.drinkFilter).length == 1) {
                        return Icon(AntDesign.pushpin);
                      } else {
                        return Icon(AntDesign.pushpino);
                      }
                    },
                  ),
                ),
                onTap: () {
                  if (pinnedItemsNotifier.pinnedItemList.toList().where((value) => value.filterType == this.widget.filterType && value.drinkFilter == this.widget.drinkFilter).length == 1) {
                    pinnedItemsNotifier.removePinnedItem(new PinnedItem(filterType: this.widget.filterType, drinkFilter: this.widget.drinkFilter));
                    _scaffoldKey.currentState.removeCurrentSnackBar();
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      duration: Duration(seconds: 3),
                      content: Text(strings.removedFromPinnedItems()),
                    ));
                  } else {
                    pinnedItemsNotifier.addPinnedItem(new PinnedItem(filterType: this.widget.filterType, drinkFilter: this.widget.drinkFilter));
                    _scaffoldKey.currentState.removeCurrentSnackBar();
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      duration: Duration(seconds: 3),
                      content: Text(strings.addedToPinnedItems()),
                    ));
                  }
                },
              ),
            ],
          ),

          this.widget.filterType == FilterType.ingredient
            ? SliverToBoxAdapter(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 8.0,
                    ),
                    Card(
                      child: IngredientCard(
                        imageUrl: ApiService.INGREDIENT_THUMBNAIL_SMALL_URL.replaceFirst('{0}', this.widget.drinkFilter),
                        ingredientStr: this.widget.drinkFilter,
                        seeDescriptionText: strings.seeDescriptionStr(),
                        onTap: () => Navigator.of(context).pushNamed(
                          ConstantsRoutes.ingredientDetailRoute,
                          arguments: IngredientDetailArguments(
                            strIngredient: this.widget.drinkFilter,
                          )
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 8.0,
                    ),
                  ],
                ),
              )
            : SliverToBoxAdapter(child: Container()),

          Consumer<DrinksFilterListNotifier>(
            builder: (context, drinksFilterList, _) {
              var drinksFilterListSearch = _searchValue.length > 0
              ? drinksFilterList?.drinksFilterList?.where((element) => element.strDrink.toLowerCase().contains(_searchValue.toLowerCase()))?.toList()
              : drinksFilterList.drinksFilterList;
              if (drinksFilterListSearch != null && drinksFilterListSearch.length > 0) {
                return SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 10.0,
                    mainAxisSpacing: 10.0,
                    childAspectRatio: 0.7,
                  ),
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return DrinkFilterCard(
                        idDrink: drinksFilterListSearch[index].idDrink,
                        drinkName: drinksFilterListSearch[index].strDrink,
                        imagePreviewUrl: '${drinksFilterListSearch[index].strDrinkThumb}/preview',
                        color: Theme.of(context).colorScheme.secondary,
                        onTap: () => Navigator.of(context).pushNamed(
                          ConstantsRoutes.drinkDetailRoute,
                          arguments: DrinkDetailArguments(
                            drinkId: drinksFilterListSearch[index].idDrink,
                          )
                        ),
                      );
                    },
                    childCount: drinksFilterListSearch.length,
                  ),
                );
              } else {
                return SliverFillRemaining(
                  child: Center(
                    child: Text(strings.noDataStr())
                  ),
                );
              }
            }
          ),

          SliverToBoxAdapter(
            child: SizedBox(
              height: 40.0,
            ),
          ),


        ],
      ),
    );
  }
}