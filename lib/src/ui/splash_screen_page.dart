
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {

  @override
  void initState() {
    Future.delayed(Duration(milliseconds: 1000),
      () => Navigator.of(context).pushNamedAndRemoveUntil(ConstantsRoutes.homeRoute, (Route<dynamic> route) => false)
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.grey[600],
            Colors.grey[900],
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          tileMode: TileMode.clamp,
        ),
      ),
      child: Center(
        child: RichText(
          text: TextSpan(
            style: Theme.of(context).primaryTextTheme.headline6,
            children: <TextSpan>[
              TextSpan(text: 'Cheeers'),
              TextSpan(text: '!!!', style: Theme.of(context).primaryTextTheme.headline6.copyWith(fontStyle: FontStyle.italic))
            ]
          ),
        )
      ),
    );
  }
}