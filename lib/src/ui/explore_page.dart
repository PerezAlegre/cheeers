import 'package:cheeers/src/core/models/enums.dart';
import 'package:cheeers/src/core/models/pinned_item.dart';
import 'package:cheeers/src/core/models/screen_arguments.dart';
import 'package:cheeers/src/core/view_models/drinks_filter_list_notifier.dart';
import 'package:cheeers/src/core/view_models/pinned_items_notifier.dart';
import 'package:cheeers/src/ui/custom_widgets/filter_card.dart';
import 'package:flutter/material.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:provider/provider.dart';
import 'package:flutter_icons/flutter_icons.dart';

class ExplorePage extends StatefulWidget {
  
  ExplorePage({Key key}) : super(key: key);

  @override
  _ExplorePageState createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);
    final drinksFilterListNotifier = Provider.of<DrinksFilterListNotifier>(context, listen: false);
    final pinnedItemsNotifier = Provider.of<PinnedItemsNotifier>(context, listen: true);

    void _onLongPressCard({@required PinnedItem pinnedItem}) {
      showModalBottomSheet(
        context: context,
        isDismissible: true,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(4.0),
            topRight: Radius.circular(4.0),
          ),
        ),
        builder: (context) => Container(
          color: Theme.of(context).dialogTheme.backgroundColor,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 16.0,
                width: double.infinity,
                child: Align(
                  alignment: Alignment.center,
                  child: Container(
                    width: 40.0,
                    height: 4.0,
                    decoration: BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.circular(2.0),
                    ),
                  ),
                ),
              ),
              ListTile(
                leading: Icon(AntDesign.delete,
                  color: Colors.redAccent
                ),
                title: Text("${strings.deleteStr()} '${pinnedItem.drinkFilter}'",
                  style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(color: Colors.redAccent)
                ),
                onTap: () {
                  pinnedItemsNotifier.removePinnedItem(new PinnedItem(filterType: pinnedItem.filterType, drinkFilter: pinnedItem.drinkFilter));
                  Navigator.of(context).pop();
                  _scaffoldKey.currentState.removeCurrentSnackBar();
                  _scaffoldKey.currentState.showSnackBar(SnackBar(
                    duration: Duration(seconds: 3),
                    content: Text(strings.removedFromPinnedItems()),
                  ));
                },
              ),
              Divider(),
              ListTile(
                leading: Icon(AntDesign.close,
                  color: Theme.of(context).primaryIconTheme.color,
                ),
                title: Text(strings.cancelStr(),
                  style: Theme.of(context).primaryTextTheme.subtitle1
                ),
                onTap: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        )
      );
    }

    return Scaffold(
      key: _scaffoldKey,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0.0,
            automaticallyImplyLeading: true,
            flexibleSpace: FlexibleSpaceBar(
              title: RichText(
                text: TextSpan(
                  style: Theme.of(context).primaryTextTheme.headline6,
                  children: <TextSpan>[
                    TextSpan(text: 'Cheeers'),
                    TextSpan(text: '!!!', style: Theme.of(context).primaryTextTheme.headline6.copyWith(fontStyle: FontStyle.italic))
                  ]
                ),
              ),
              titlePadding: EdgeInsetsDirectional.only(start: 16.0, bottom: 8.0),
              centerTitle: false,
              collapseMode: CollapseMode.parallax,
              background: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Theme.of(context).colorScheme.primaryVariant,
                      Theme.of(context).colorScheme.primary,
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    tileMode: TileMode.clamp,
                  ),
                ),
              ),
            ),
            expandedHeight: 140.0,
            floating: false,
            pinned: true,
            snap: false,
            actions: <Widget>[
              GestureDetector(
                child: Padding(
                  padding: EdgeInsets.only(right: 16.0),
                  child: Icon(AntDesign.setting),
                ),
                onTap: () => Navigator.of(context).pushNamed(ConstantsRoutes.settingsRoute),
              ),
            ],
          ),

          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(strings.ingredientsStr(),
                    style: Theme.of(context).primaryTextTheme.subtitle2,
                  ),
                  GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(strings.seeAllStr(),
                          style: Theme.of(context).primaryTextTheme.bodyText2
                        ),
                        SizedBox(
                          width: 4.0
                        ),
                        Icon(AntDesign.arrowright, 
                          size: Theme.of(context).primaryTextTheme.bodyText2.fontSize,
                          color: Theme.of(context).primaryTextTheme.bodyText2.color
                        )
                      ],
                    ),
                    onTap: () => Navigator.of(context).pushNamed(ConstantsRoutes.allIngredientsRoute),
                  )
                ],
              ),
            ),
          ),

          SliverToBoxAdapter(
            child: Consumer<PinnedItemsNotifier>(
              builder: (context, pinnedItems, _) => Container(
                height: 120.0,
                child: ListView.builder(
                  key: PageStorageKey('IngredientsList'),
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  itemCount: pinnedItems.pinnedItemListIngredients.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: pinnedItems.pinnedItemListIngredients.length > 1
                        ? index == pinnedItems.pinnedItemListIngredients.length - 1
                          ? EdgeInsets.only(right: 16.0)
                          : index == 0
                            ? EdgeInsets.only(left: 16.0, right: 8.0)
                            : EdgeInsets.only(right: 8.0)
                        : EdgeInsets.only(left: 16.0),
                      child: FilterCard(
                        title: pinnedItems.pinnedItemListIngredients[index].drinkFilter,
                        color: Theme.of(context).colorScheme.secondary,
                        onTap: () {
                          drinksFilterListNotifier.getAllDrinksList(
                            filterType: FilterType.ingredient,
                            filterValue: pinnedItems.pinnedItemListIngredients[index].drinkFilter
                          );
                          Navigator.of(context).pushNamed(
                            ConstantsRoutes.drinksFilterRoute,
                            arguments: DrinksFilterListArguments(
                              filterType: FilterType.ingredient,
                              drinkFilter: pinnedItems.pinnedItemListIngredients[index].drinkFilter
                            )
                          );
                        },
                        onLongPress: () => _onLongPressCard(
                          pinnedItem: PinnedItem(
                            filterType: FilterType.ingredient,
                            drinkFilter: pinnedItems.pinnedItemListIngredients[index].drinkFilter
                          )
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),

          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(strings.categoriesStr(),
                    style: Theme.of(context).primaryTextTheme.subtitle2,
                  ),
                  GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(strings.seeAllStr(),
                          style: Theme.of(context).primaryTextTheme.bodyText2
                        ),
                        SizedBox(
                          width: 4.0
                        ),
                        Icon(AntDesign.arrowright, 
                          size: Theme.of(context).primaryTextTheme.bodyText2.fontSize,
                          color: Theme.of(context).primaryTextTheme.bodyText2.color
                        )
                      ],
                    ),
                    onTap: () => Navigator.of(context).pushNamed(ConstantsRoutes.allCategoriesRoute),
                  )
                ],
              ),
            ),
          ),

          SliverToBoxAdapter(
            child: Consumer<PinnedItemsNotifier>(
              builder: (context, pinnedItems, _) => Container(
                height: 120.0,
                child: ListView.builder(
                  key: PageStorageKey('CategoriesList'),
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  itemCount: pinnedItems.pinnedItemListCategories.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: pinnedItems.pinnedItemListCategories.length > 1
                        ? index == pinnedItems.pinnedItemListCategories.length - 1
                          ? EdgeInsets.only(right: 16.0)
                          : index == 0
                            ? EdgeInsets.only(left: 16.0, right: 8.0)
                            : EdgeInsets.only(right: 8.0)
                        : EdgeInsets.only(left: 16.0),
                      child: FilterCard(
                        title: pinnedItems.pinnedItemListCategories[index].drinkFilter,
                        color: Theme.of(context).colorScheme.secondary,
                        onTap: () {
                          drinksFilterListNotifier.getAllDrinksList(
                            filterType: FilterType.category,
                            filterValue: pinnedItems.pinnedItemListCategories[index].drinkFilter
                          );
                          Navigator.of(context).pushNamed(
                            ConstantsRoutes.drinksFilterRoute,
                            arguments: DrinksFilterListArguments(
                              filterType: FilterType.category,
                              drinkFilter: pinnedItems.pinnedItemListCategories[index].drinkFilter
                            )
                          );
                        },
                        onLongPress: () => _onLongPressCard(
                          pinnedItem: PinnedItem(
                            filterType: FilterType.category,
                            drinkFilter: pinnedItems.pinnedItemListCategories[index].drinkFilter
                          )
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),

          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(strings.glassesStr(),
                    style: Theme.of(context).primaryTextTheme.subtitle2,
                  ),
                  GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(strings.seeAllStr(),
                          style: Theme.of(context).primaryTextTheme.bodyText2
                        ),
                        SizedBox(
                          width: 4.0
                        ),
                        Icon(AntDesign.arrowright, 
                          size: Theme.of(context).primaryTextTheme.bodyText2.fontSize,
                          color: Theme.of(context).primaryTextTheme.bodyText2.color
                        )
                      ],
                    ),
                    onTap: () => Navigator.of(context).pushNamed(ConstantsRoutes.allGlassesRoute),
                  )
                ],
              ),
            ),
          ),

          SliverToBoxAdapter(
            child: Consumer<PinnedItemsNotifier>(
              builder: (context, pinnedItems, _) => Container(
                height: 120.0,
                child: ListView.builder(
                  key: PageStorageKey('GlassesList'),
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  itemCount: pinnedItems.pinnedItemListGlasses.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: pinnedItems.pinnedItemListGlasses.length > 1
                        ? index == pinnedItems.pinnedItemListGlasses.length - 1
                          ? EdgeInsets.only(right: 16.0)
                          : index == 0
                            ? EdgeInsets.only(left: 16.0, right: 8.0)
                            : EdgeInsets.only(right: 8.0)
                        : EdgeInsets.only(left: 16.0),
                      child: FilterCard(
                        title: pinnedItems.pinnedItemListGlasses[index].drinkFilter,
                        color: Theme.of(context).colorScheme.secondary,
                        onTap: () {
                          drinksFilterListNotifier.getAllDrinksList(
                            filterType: FilterType.glass,
                            filterValue: pinnedItems.pinnedItemListGlasses[index].drinkFilter
                          );
                          Navigator.of(context).pushNamed(
                            ConstantsRoutes.drinksFilterRoute,
                            arguments: DrinksFilterListArguments(
                              filterType: FilterType.glass,
                              drinkFilter: pinnedItems.pinnedItemListGlasses[index].drinkFilter
                            )
                          );
                        },
                        onLongPress: () => _onLongPressCard(
                          pinnedItem: PinnedItem(
                            filterType: FilterType.glass,
                            drinkFilter: pinnedItems.pinnedItemListGlasses[index].drinkFilter
                          )
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),

          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(strings.alcoholicsStr(),
                    style: Theme.of(context).primaryTextTheme.subtitle2,
                  ),
                  GestureDetector(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(strings.seeAllStr(),
                          style: Theme.of(context).primaryTextTheme.bodyText2
                        ),
                        SizedBox(
                          width: 4.0
                        ),
                        Icon(AntDesign.arrowright, 
                          size: Theme.of(context).primaryTextTheme.bodyText2.fontSize,
                          color: Theme.of(context).primaryTextTheme.bodyText2.color
                        )
                      ],
                    ),
                    onTap: () => Navigator.of(context).pushNamed(ConstantsRoutes.allAlcoholicsRoute),
                  )
                ],
              ),
            ),
          ),

          // SliverToBoxAdapter(
          //   child: Consumer<AlcoholicListNotifier>(
          //     builder: (context, alcoholicList, _) => Container(
          //       height: 120.0,
          //       child: ListView.builder(
          //         key: PageStorageKey('AlcoholicList'),
          //         scrollDirection: Axis.horizontal,
          //         physics: BouncingScrollPhysics(),
          //         itemCount: alcoholicList.suggestedAlcoholics.length,
          //         itemBuilder: (BuildContext context, int index) {
          //           return Padding(
          //             padding: index == alcoholicList.suggestedAlcoholics.length - 1
          //               ? EdgeInsets.only(right: 16.0)
          //               : index == 0
          //                 ? EdgeInsets.only(left: 16.0, right: 8.0)
          //                 : EdgeInsets.only(right: 8.0),
          //             child: FilterCard(
          //               title: alcoholicList.suggestedAlcoholics[index].strAlcoholic,
          //               color: Theme.of(context).colorScheme.secondary,
          //               onTap: () {
          //                 drinksFilterListNotifier.getAllDrinksList(
          //                   filterType: FilterType.alcoholic,
          //                   filterValue: alcoholicList.suggestedAlcoholics[index].strAlcoholic
          //                 );
          //                 Navigator.of(context).pushNamed(
          //                   ConstantsRoutes.drinksFilterRoute,
          //                   arguments: DrinksFilterListArguments(
          //                     filterType: FilterType.alcoholic,
          //                     drinkFilter: alcoholicList.suggestedAlcoholics[index].strAlcoholic
          //                   )
          //                 );
          //               },
          //               onLongPress: () => _onLongPressCard(
          //                 pinnedItem: PinnedItem(
          //                   filterType: FilterType.alcoholic,
          //                   drinkFilter: alcoholicList.suggestedAlcoholics[index].strAlcoholic
          //                 )
          //               ),
          //             ),
          //           );
          //         },
          //       ),
          //     ),
          //   ),
          // ),

          SliverToBoxAdapter(
            child: Consumer<PinnedItemsNotifier>(
              builder: (context, pinnedItems, _) => Container(
                height: 120.0,
                child: ListView.builder(
                  key: PageStorageKey('AlcoholicList'),
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  itemCount: pinnedItems.pinnedItemListAlcoholic.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Padding(
                      padding: pinnedItems.pinnedItemListAlcoholic.length > 1
                        ? index == pinnedItems.pinnedItemListAlcoholic.length - 1
                          ? EdgeInsets.only(right: 16.0)
                          : index == 0
                            ? EdgeInsets.only(left: 16.0, right: 8.0)
                            : EdgeInsets.only(right: 8.0)
                        : EdgeInsets.only(left: 16.0),
                      child: FilterCard(
                        title: pinnedItems.pinnedItemListAlcoholic[index].drinkFilter,
                        color: Theme.of(context).colorScheme.secondary,
                        onTap: () {
                          drinksFilterListNotifier.getAllDrinksList(
                            filterType: FilterType.alcoholic,
                            filterValue: pinnedItems.pinnedItemListAlcoholic[index].drinkFilter
                          );
                          Navigator.of(context).pushNamed(
                            ConstantsRoutes.drinksFilterRoute,
                            arguments: DrinksFilterListArguments(
                              filterType: FilterType.alcoholic,
                              drinkFilter: pinnedItems.pinnedItemListAlcoholic[index].drinkFilter
                            )
                          );
                        },
                        onLongPress: () => _onLongPressCard(
                          pinnedItem: PinnedItem(
                            filterType: FilterType.alcoholic,
                            drinkFilter: pinnedItems.pinnedItemListAlcoholic[index].drinkFilter
                          )
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),

          SliverToBoxAdapter(
            child: SizedBox(
              height: 40.0,
            ),
          ),
          

        ],
      ),
    );
  }
}