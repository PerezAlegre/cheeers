import 'dart:async';

import 'package:cheeers/src/core/models/drink.dart';
import 'package:cheeers/src/core/models/screen_arguments.dart';
import 'package:cheeers/src/core/view_models/drink_provider.dart';
import 'package:cheeers/src/core/view_models/favorites_list_notifier.dart';
import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class DrinkDetailPage extends StatefulWidget {

  @override
  _DrinkDetailPageState createState() => _DrinkDetailPageState();
}

class _DrinkDetailPageState extends State<DrinkDetailPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _showImage;
  bool _doubleTappedOnImageWithText;
  bool _doubleTappedOnImage;
  final int _favoriteDuration = 1500;

  @override
  void initState() {
    _showImage = false;
    _doubleTappedOnImageWithText = false;
    _doubleTappedOnImage = false;
    super.initState();
  }
// Offset _tapPosition;

// void _handleTapDown(TapDownDetails details) {
//   final RenderBox referenceBox = context.findRenderObject();
//   setState(() {
//     _tapPosition = referenceBox.globalToLocal(details.globalPosition);
//   });
// }

// @override
// Widget build(BuildContext context) {
//   return new GestureDetector(
//      /* ... */
//      onTapDown: _handleTapDown,
//   );
// }

  @override
  Widget build(BuildContext context) {

    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);
    final favoritesListNotifier = Provider.of<FavoritesListNotifier>(context, listen: true);

    final double safeHeight = MediaQuery.of(context).padding.top;
    final double safeHeightForIcons = safeHeight + 16.0;

    Widget _ingredientAndMeasure({@required String ingredient, @required String measure}) {
      return ingredient != null && ingredient.isNotEmpty
        ? Container(
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 2.0),
          width: MediaQuery.of(context).size.width - 32.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                child: Text(ingredient,
                  maxLines: 100,
                  style: Theme.of(context).primaryTextTheme.bodyText2.copyWith(
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w500
                  ),
                ),
                onTap: () => Navigator.of(context).pushNamed(
                  ConstantsRoutes.ingredientDetailRoute,
                  arguments: IngredientDetailArguments(
                    strIngredient: ingredient,
                  )
                ),
              ),
              Text(measure != null ? ':  $measure' : '',
                maxLines: 100,
                style: Theme.of(context).primaryTextTheme.bodyText2,
              )
            ],
          ),
        )
        : Container();
    }

    void _addFavoriteFromDoubleTap(Drink drink) {
      if (favoritesListNotifier.favoritesList.toList().where((value) => value.idDrink == drink.idDrink).length == 0) {
        favoritesListNotifier.addFavorite(drink);
        _scaffoldKey.currentState.removeCurrentSnackBar();
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          duration: Duration(seconds: 3),
          content: Text(strings.addedToFavorites()),
        ));
      }
    }

    void _animateFavoriteOnImage(Drink drink) {
      setState(() {
        _doubleTappedOnImage = true;
      });
      _addFavoriteFromDoubleTap(drink);
      Timer(Duration(milliseconds: _favoriteDuration), () {
        setState(() {
          _doubleTappedOnImage = false;
        });
      });
    }

    void _animateFavoriteOnImageWithText(Drink drink) {
      setState(() {
        _doubleTappedOnImageWithText = true;
      });
      _addFavoriteFromDoubleTap(drink);
      Timer(Duration(milliseconds: _favoriteDuration), () {
        setState(() {
          _doubleTappedOnImageWithText = false;
        });
      });
    }

    return Consumer<DrinkProvider>(
      builder: (context, drinkProvider, _) {
        if (drinkProvider.drink == null) {
          return Scaffold(
            appBar: AppBar(
              leading: GestureDetector(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Icon(AntDesign.arrowleft),
                  ),
                onTap: () => Navigator.of(context).pop(),
              ),
            ),
          );
        }
        else {
          return _showImage
          ? Scaffold(
            key: _scaffoldKey,
            backgroundColor: Theme.of(context).primaryColor,
            body: Stack(
              children: <Widget>[
                Hero(
                  tag: drinkProvider.drink.idDrink,
                  child: GestureDetector(
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(drinkProvider.drink.strDrinkThumb),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    onTap: () {
                      setState(() {
                        _showImage = false;
                      });
                    },
                    onDoubleTap: () => _animateFavoriteOnImage(drinkProvider.drink),
                  ),
                ),
                _doubleTappedOnImage
                ? Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 150.0,
                    width: 150.0,
                    child: FlareActor('assets/Favorite.flr',
                      animation: 'Favorite',
                      fit: BoxFit.contain,
                      alignment: Alignment.center,
                    ),
                  ),
                )
                : Container(),
              ],
            ),
          )
          : Scaffold(
            key: _scaffoldKey,
            backgroundColor: Theme.of(context).primaryColor,
            body: Stack(
              children: <Widget>[
                Hero(
                  tag: drinkProvider.drink.idDrink,
                  child: GestureDetector(
                    child: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(drinkProvider.drink.strDrinkThumb),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Theme.of(context).primaryColor.withOpacity(1.0),
                              Theme.of(context).primaryColor.withOpacity(0.6),
                              Theme.of(context).primaryColor.withOpacity(0.1),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                            tileMode: TileMode.clamp,
                          ),
                        ),
                      ),
                    ),
                    onTap: () {
                      setState(() {
                        _showImage = true;
                      });
                    },
                    onDoubleTap: () => _animateFavoriteOnImageWithText(drinkProvider.drink),
                  ),
                ),
                _doubleTappedOnImageWithText
                ? Align(
                  alignment: AlignmentDirectional(0.0, -0.5),
                  child: SizedBox(
                    height: 150.0,
                    width: 150.0,
                    child: FlareActor('assets/Favorite.flr',
                      animation: 'Favorite',
                      fit: BoxFit.contain,
                      alignment: Alignment.center,
                    ),
                  ),
                )
                : Container(),
                Positioned(
                  top: safeHeightForIcons,
                  left: 16.0,
                  child: GestureDetector(
                    child: ClipOval(
                      clipBehavior: Clip.antiAlias,
                      child: Container(
                        width: 40.0,
                        height: 40.0,
                        color: Theme.of(context).primaryColor.withOpacity(0.5),
                        child: Icon(AntDesign.arrowleft)
                      ),
                    ),
                    onTap: () => Navigator.of(context).pop(),
                  ),
                ),
                Positioned(
                  top: safeHeightForIcons,
                  right: 16.0,
                  child: GestureDetector(
                    child: ClipOval(
                      clipBehavior: Clip.antiAlias,
                      child: Container(
                        width: 40.0,
                        height: 40.0,
                        color: Theme.of(context).primaryColor.withOpacity(0.5),
                        child: Consumer<FavoritesListNotifier>(
                          builder: (context, favorites, _) {
                            if (favorites.favoritesList.toList().where((value) => value.idDrink == drinkProvider.drink.idDrink).length == 1) {
                              return Icon(AntDesign.heart,
                                color: Theme.of(context).colorScheme.secondary
                              );
                            } else {
                              return Icon(AntDesign.hearto,
                                color: Theme.of(context).colorScheme.secondary
                              );
                            }
                          }
                        ),
                      ),
                    ),
                    onTap: () {
                      if (favoritesListNotifier.favoritesList.toList().where((value) => value.idDrink == drinkProvider.drink.idDrink).length == 1) {
                        favoritesListNotifier.removeFavorite(drinkProvider.drink);
                        _scaffoldKey.currentState.removeCurrentSnackBar();
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          duration: Duration(seconds: 3),
                          content: Text(strings.removedFromFavorites()),
                        ));
                      } else {
                        favoritesListNotifier.addFavorite(drinkProvider.drink);
                        _scaffoldKey.currentState.removeCurrentSnackBar();
                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                          duration: Duration(seconds: 3),
                          content: Text(strings.addedToFavorites()),
                        ));
                      }
                    },
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).size.height * 0.5,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.5,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 16.0),
                            width: MediaQuery.of(context).size.width - 32.0,
                            child: Text(drinkProvider.drink.strDrink,
                              style: Theme.of(context).primaryTextTheme.headline6,
                              maxLines: 3,
                              textAlign: TextAlign.left,
                            ),
                          ),

                          Container(
                            height: 60.0,
                            width: MediaQuery.of(context).size.width,
                            child: ListView(
                              scrollDirection: Axis.horizontal,
                              physics: BouncingScrollPhysics(),
                              children: <Widget>[
                                drinkProvider.drink.strCategory != null 
                                  ? Padding(
                                    padding: EdgeInsets.only(left: 16.0),
                                    child: Chip(
                                      label: Text(drinkProvider.drink.strCategory,
                                        style: Theme.of(context).primaryTextTheme.bodyText2,
                                      ),
                                      backgroundColor: Theme.of(context).primaryColor.withOpacity(0.8),
                                    ),
                                  )
                                  : Container(),
                                drinkProvider.drink.strGlass != null 
                                  ? Padding(
                                    padding: EdgeInsets.only(left: 16.0),
                                    child: Chip(
                                      label: Text(drinkProvider.drink.strGlass,
                                        style: Theme.of(context).primaryTextTheme.bodyText2,
                                      ),
                                      backgroundColor: Theme.of(context).primaryColor.withOpacity(0.8),
                                    ),
                                  )
                                  : Container(),
                                drinkProvider.drink.strAlcoholic != null 
                                  ? Padding(
                                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                                    child: Chip(
                                      label: Text(drinkProvider.drink.strAlcoholic,
                                        style: Theme.of(context).primaryTextTheme.bodyText2,
                                      ),
                                      backgroundColor: Theme.of(context).primaryColor.withOpacity(0.8),
                                    ),
                                  )
                                  : Container(),
                              ],
                            ),
                          ),
                          
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16.0),
                            child: Text(strings.ingredientsStr(),
                              style: Theme.of(context).primaryTextTheme.subtitle2,
                              textAlign: TextAlign.left,
                            ),
                          ),
                          
                          SizedBox(
                            height: 5.0,
                          ),

                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient1,
                            measure: drinkProvider.drink.strMeasure1,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient2,
                            measure: drinkProvider.drink.strMeasure2,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient3,
                            measure: drinkProvider.drink.strMeasure3,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient4,
                            measure: drinkProvider.drink.strMeasure4,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient5,
                            measure: drinkProvider.drink.strMeasure5,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient6,
                            measure: drinkProvider.drink.strMeasure6,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient7,
                            measure: drinkProvider.drink.strMeasure7,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient8,
                            measure: drinkProvider.drink.strMeasure8,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient9,
                            measure: drinkProvider.drink.strMeasure9,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient10,
                            measure: drinkProvider.drink.strMeasure10,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient11,
                            measure: drinkProvider.drink.strMeasure11,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient12,
                            measure: drinkProvider.drink.strMeasure12,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient13,
                            measure: drinkProvider.drink.strMeasure13,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient14,
                            measure: drinkProvider.drink.strMeasure14,
                          ),
                          _ingredientAndMeasure(
                            ingredient: drinkProvider.drink.strIngredient15,
                            measure: drinkProvider.drink.strMeasure15,
                          ),

                          SizedBox(
                            height: 10.0,
                          ),
                          
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16.0),
                            child: Text(strings.instructionsStr(),
                              style: Theme.of(context).primaryTextTheme.subtitle2,
                              textAlign: TextAlign.left,
                            ),
                          ),
                          
                          SizedBox(
                            height: 5.0,
                          ),
                          
                          Container(
                            padding: EdgeInsets.symmetric(horizontal: 16.0),
                            width: MediaQuery.of(context).size.width - 32.0,
                            child: Text(drinkProvider.drink.strInstructions,
                              maxLines: 100,
                              style: Theme.of(context).primaryTextTheme.bodyText2,
                            ),
                          ),
                          
                          SizedBox(
                            height: 16.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        }
      }
    );
  }
}