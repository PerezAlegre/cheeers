import 'package:cheeers/src/core/models/screen_arguments.dart';
import 'package:cheeers/src/core/view_models/favorites_list_notifier.dart';
import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/ui/custom_widgets/drink_filter_card.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FavoritesPage extends StatelessWidget {
  
  FavoritesPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            elevation: 0.0,
            automaticallyImplyLeading: true,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(strings.favoritesStr(),
                style: Theme.of(context).primaryTextTheme.headline6,
              ),
              titlePadding: EdgeInsetsDirectional.only(start: 16.0, bottom: 8.0),
              centerTitle: false,
              collapseMode: CollapseMode.parallax,
              background: Container(
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Theme.of(context).colorScheme.primaryVariant,
                      Theme.of(context).colorScheme.primary,
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    tileMode: TileMode.clamp,
                  ),
                ),
              ),
            ),
            expandedHeight: 140.0,
            floating: false,
            pinned: true,
            snap: false,
          ),

          Consumer<FavoritesListNotifier>(
            builder: (context, favorites, _) {
              if (favorites.favoritesList != null && favorites.favoritesList.length > 0) {
                return SliverGrid(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    crossAxisSpacing: 10.0,
                    mainAxisSpacing: 10.0,
                    childAspectRatio: 0.7,
                  ),
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return DrinkFilterCard(
                        idDrink: favorites.favoritesList[index].idDrink,
                        drinkName: favorites.favoritesList[index].strDrink,
                        imagePreviewUrl: '${favorites.favoritesList[index].strDrinkThumb}/preview',
                        color: Theme.of(context).colorScheme.secondary,
                        onTap: () {
                          Navigator.of(context).pushNamed(
                            ConstantsRoutes.favoriteDrinkRoute,
                            arguments: FavoriteDrinkArguments(
                              initialPage: index
                            )
                          );
                        },
                      );
                    },
                    childCount: favorites.favoritesList.length,
                  ),
                );
              } else {
                return SliverFillRemaining(
                  child: Center(
                    child: Text(strings.noDataStr())
                  ),
                );
              }
            }
          ),

          SliverToBoxAdapter(
            child: SizedBox(
              height: 40.0,
            ),
          ),

          
        ],
      )
    );
  }
}