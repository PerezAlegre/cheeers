import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

class AppInfoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final languageNotifier = Provider.of<LanguageNotifier>(context, listen: true);
    ConstantsStrings strings = new ConstantsStrings(languageNotifier.language);

    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Icon(AntDesign.arrowleft),
            ),
          onTap: () => Navigator.of(context).pop(),
        ),
        title: Text(strings.appInfoStr(),
          style: Theme.of(context).primaryTextTheme.subtitle2,
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('${strings.versionStr()}: 0.0.1',
                style: Theme.of(context).primaryTextTheme.bodyText1,
              ),
              SizedBox(
                height: 8.0
              ),
              Text('${strings.developedByStr()}: Adriel Pérez Alegre',
                style: Theme.of(context).primaryTextTheme.bodyText2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}