import 'package:cheeers/src/core/models/screen_arguments.dart';
import 'package:cheeers/src/core/view_models/drink_provider.dart';
import 'package:cheeers/src/core/view_models/ingredient_provider.dart';
import 'package:cheeers/src/core/view_models/random_drink_provider.dart';
import 'package:cheeers/src/ui/all_alcoholic_page.dart';
import 'package:cheeers/src/ui/all_categories_page.dart';
import 'package:cheeers/src/ui/all_glasses_page.dart';
import 'package:cheeers/src/ui/all_ingredients_page.dart';
import 'package:cheeers/src/ui/app_info_page.dart';
import 'package:cheeers/src/ui/drink_detail_page.dart';
import 'package:cheeers/src/ui/drinks_filter_page.dart';
import 'package:cheeers/src/ui/explore_page.dart';
import 'package:cheeers/src/ui/favorite_drink_page.dart';
import 'package:cheeers/src/ui/favorites_page.dart';
import 'package:cheeers/src/ui/home_page.dart';
import 'package:cheeers/src/ui/ingredient_detail_page.dart';
import 'package:cheeers/src/ui/random_drink_detail_page.dart';
import 'package:cheeers/src/ui/search_page.dart';
import 'package:cheeers/src/ui/settings_page.dart';
import 'package:cheeers/src/ui/splash_screen_page.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Router {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case ConstantsRoutes.splashScreenRoute:
        return MaterialPageRoute(builder: (_) => SplashScreenPage());
      case ConstantsRoutes.homeRoute:
        return MaterialPageRoute(builder: (_) => HomePage());
      case ConstantsRoutes.exploreRoute:
        return MaterialPageRoute(builder: (_) => ExplorePage());
      case ConstantsRoutes.searchRoute:
        return MaterialPageRoute(builder: (_) => SearchPage());
      case ConstantsRoutes.favoritesRoute:
        return MaterialPageRoute(builder: (_) => FavoritesPage());
      case ConstantsRoutes.settingsRoute:
        return MaterialPageRoute(builder: (_) => SettingsPage());
      case ConstantsRoutes.appInfoRoute:
        return MaterialPageRoute(builder: (_) => AppInfoPage());
      case ConstantsRoutes.allAlcoholicsRoute:
        return MaterialPageRoute(builder: (_) => AllAlcoholicPage());
      case ConstantsRoutes.allCategoriesRoute:
        return MaterialPageRoute(builder: (_) => AllCategoriesPage());
      case ConstantsRoutes.allGlassesRoute:
        return MaterialPageRoute(builder: (_) => AllGlassesPage());
      case ConstantsRoutes.allIngredientsRoute:
        return MaterialPageRoute(builder: (_) => AllIngredientsPage());
      case ConstantsRoutes.drinksFilterRoute:
        final DrinksFilterListArguments args = settings.arguments;
        return MaterialPageRoute(builder: (_) => DrinksFilterPage(
          key: PageStorageKey(args.drinkFilter.toString()),
          filterType: args.filterType,
          drinkFilter: args.drinkFilter,
        ));
      case ConstantsRoutes.drinkDetailRoute:
        final DrinkDetailArguments args = settings.arguments;
        return MaterialPageRoute(builder: (_) => ChangeNotifierProvider<DrinkProvider>(
          create: (_) => DrinkProvider(args.drinkId),
          child: DrinkDetailPage(),
        ));
      case ConstantsRoutes.favoriteDrinkRoute:
        final FavoriteDrinkArguments args = settings.arguments;
        return MaterialPageRoute(builder: (_) => FavoriteDrinkPage(
          initialPage: args.initialPage,
        ));
      case ConstantsRoutes.ingredientDetailRoute:
        final IngredientDetailArguments args = settings.arguments;
        return MaterialPageRoute(builder: (_) => ChangeNotifierProvider<IngredientProvider>(
          create: (_) => IngredientProvider(args.strIngredient),
          child: IngredientDetailPage(),
        ));
      case ConstantsRoutes.randomDrinkRoute:
        return MaterialPageRoute(builder: (_) => ChangeNotifierProvider<RandomDrinkProvider>(
          create: (_) => RandomDrinkProvider(),
          child: RandomDrinkDetailPage(),
        ));
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}')
            ),
          )
        );
    }
  }

}