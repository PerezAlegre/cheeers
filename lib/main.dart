import 'package:cheeers/router.dart';
import 'package:cheeers/src/core/view_models/alcoholic_list_notifier.dart';
import 'package:cheeers/src/core/view_models/categories_list_notifier.dart';
import 'package:cheeers/src/core/view_models/drinks_filter_list_notifier.dart';
import 'package:cheeers/src/core/view_models/favorites_list_notifier.dart';
import 'package:cheeers/src/core/view_models/glasses_list_notifier.dart';
import 'package:cheeers/src/core/view_models/ingredients_list_notifier.dart';
import 'package:cheeers/src/core/view_models/language_notifier.dart';
import 'package:cheeers/src/core/view_models/pinned_items_notifier.dart';
import 'package:cheeers/src/core/view_models/search_notifier.dart';
import 'package:cheeers/src/core/view_models/theme_notifier.dart';
import 'package:cheeers/src/core/models/theme.dart';
import 'package:cheeers/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeNotifier>(
          create: (context) => ThemeNotifier(),
        ),
        ChangeNotifierProvider<LanguageNotifier>(
          create: (context) => LanguageNotifier(),
        ),
        ChangeNotifierProvider<PinnedItemsNotifier>(
          create: (context) => PinnedItemsNotifier(),
        ),
        ChangeNotifierProvider<AlcoholicListNotifier>(
          create: (context) => AlcoholicListNotifier(),
        ),
        ChangeNotifierProvider<CategoriesListNotifier>(
          create: (context) => CategoriesListNotifier(),
        ),
        ChangeNotifierProvider<GlassesListNotifier>(
          create: (context) => GlassesListNotifier(),
        ),
        ChangeNotifierProvider<IngredientsListNotifier>(
          create: (context) => IngredientsListNotifier(),
        ),
        ChangeNotifierProvider<DrinksFilterListNotifier>(
          create: (context) => DrinksFilterListNotifier(),
        ),
        ChangeNotifierProvider(
          create: (context) => FavoritesListNotifier(),
        ),
        ChangeNotifierProvider<SearchNotifier>(
          create: (context) => SearchNotifier(),
        ),
      ],
      child: MaterialAppWithTheme(),
    );
  }
}

class MaterialAppWithTheme extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context, listen: true);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: AppTheme.lightTheme,
      darkTheme: AppTheme.darkTheme,
      themeMode: themeNotifier.themeMode,
      onGenerateRoute: Router.generateRoute,
      initialRoute: ConstantsRoutes.splashScreenRoute,
    );
  }
}